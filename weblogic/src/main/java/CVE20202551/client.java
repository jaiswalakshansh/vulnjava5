package CVE20202551;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

/**
 * @ClassName: client
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/8/4 17:52
 * @Version: v1.0.0
 * @Description:
 **/
public class client {
    public static void main(String[] args) {
        try {
            Socket s = new Socket("127.0.0.1",8888);

            //构建IO
            InputStream is = s.getInputStream();
            OutputStream os = s.getOutputStream();

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
            //向服务器端发送一条消息
            String demo = "刘辰俊\n";
            String hexDemo ="47494F5030313032303030333030303030303137303030303030303230303030303030303030303030303030303030624E616D6553657276696365";

            bw.write(demo);
//            bw.write(demo + "测试客户端和服务器通信，服务器接收到消息返回到客户端\n");

            bw.flush();

            //读取服务器返回的消息
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String mess = br.readLine();
            System.out.println("服务器："+mess);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
