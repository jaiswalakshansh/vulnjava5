package util;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: SocketUtil
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/12/11 18:28
 * @Version: v1.0.0
 * @Description:
 **/
public class SocketUtil {
    static String Regex = "0042454108010300000000(.*?)6572766572";
    static String Regex2 = "0042454108010300000000(.*?)0044524d493a7765626c6f6769632e636c75737465722e73696e676c65746f6e2e436c75737465724d617374657252656d6f74653a30303030303030303030303030303030(.*?)002c00";

    /**
     * @Description:
     * @param sendMessage 发送信息hexString字符串
     * @param socket    socket 连接
     * @return: 返回读取到的内容
     */
    public static byte[] sendSocket(String sendMessage, Socket socket) throws Exception {
        OutputStream out = socket.getOutputStream();
        InputStream is = socket.getInputStream();
        out.write(HexStringUtil.hexStrToBinaryStr(sendMessage));
        out.flush();
        byte[] bytes = new byte[20480];
        int length = is.read(bytes);
        return Arrays.copyOfRange(bytes, 0,length);
    }
    /**
     * @Description:
     * @param str   代修改字符串
     * @param strLength  指定长度
     *
     * @return: 修改后的字符串
     */
    public static String addZeroForNum(String str, int strLength) {
        int strLen = str.length();
        if (strLen < strLength) {
            while (strLen < strLength) {
                StringBuffer sb = new StringBuffer();
//                sb.append("0").append(str);// 左补0
                 sb.append(str).append("0");//右补0
                str = sb.toString();
                strLen = str.length();
            }
        }
        return str;
    }
    public static String subZeroForNum(String str, int strLength) {
        String r = str.substring(0,strLength);
        return r;
    }
    /**
     * @Description:
     * @param Input 返回的hexstr
     *
     * @return: ServerName
     */
    public static String RegexServer(String Input){
        String result = "";
        Pattern pattern = Pattern.compile(Regex);
        Matcher matcher = pattern.matcher(Input);
        if (matcher.find()){
            result = matcher.group();
            if (result.length()<60){
                result = addZeroForNum(result,60);
            }
        }


        return result;
    }
   /**
    * @Description:
    * @param Input 返回的hexstr
    *
    * @return: ServerNameStubData
    */
    public static String RegexStubData(String Input){
        String result = "";
        String regex = RegexServer(Input);
        String regex2 = regex + "(.*?)002c00";
        Pattern pattern = Pattern.compile(regex2);
        Matcher matcher = pattern.matcher(Input);
        if (matcher.find()){
            result = matcher.group();
        }
        // 如果匹配结果小于272，继续匹配
        // 正常情况下第一次匹配结果小于272
        if (result.length() < 272){
            if (matcher.find()){
                result = matcher.group();
            }

        }
        // 删除多余的字符
        if (result.length() > 272){
            result = result.substring(0,272);
        }

        return result;
    }


}
