#!/bin/sh

# Set WebLogic Home
DIRNAME=`dirname $0`/..
WL_HOME=`cd ${DIRNAME} && pwd`
export WL_HOME

"/u01/app/oracle/middleware/utils/uninstall/uninstall.sh" $*

exit $?
