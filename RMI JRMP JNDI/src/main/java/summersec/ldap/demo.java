package summersec.ldap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

/**
 * @ClassName: demo
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/9/5 13:37
 * @Version: v1.0.0
 * @Description:
 **/
public class demo {
    public static void main(String[] args) {

        System.out.println("java.version = " + System.getProperty("java.version"));
        System.out.println("sun.arch.data.model = " + System.getProperty("sun.arch.data.model"));

        String ldap = "ldap://127.0.0.1:1389/Calc";
        Context context = null;
        try {
            context = new InitialContext();
            context.lookup(ldap);

        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
