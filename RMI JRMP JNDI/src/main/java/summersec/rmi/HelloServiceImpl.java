package summersec.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @ClassName: HelloServiceImpl
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/4/25 16:16
 * @Version: v1.0.0
 * @Description:
 **/
public class HelloServiceImpl extends UnicastRemoteObject implements HelloService {

    protected HelloServiceImpl() throws RemoteException{

    }

    public String sayHello() throws RemoteException {
        System.out.println("hello i am service! ");
        return "hello i am service! ";
    }
}
