package summersec.rmi;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * @ClassName: AppD
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/4/25 17:26
 * @Version: v1.0.0
 * @Description:
 **/
public class AppD {
    public static void main(String[] args) {

        try {
            Object context = new InitialContext().lookup("rmi://127.0.0.1:1099/hello");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
