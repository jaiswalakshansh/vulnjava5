package summersec.rmi;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @ClassName: App
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/4/25 16:19
 * @Version: v1.0.0
 * @Description:
 **/
public class AppA {
    public static void main(String[] args) {

        try {
            Registry registry = LocateRegistry.createRegistry(1099 );
            registry.bind("hello", new HelloServiceImpl());
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            e.printStackTrace();
        }
    }
}
