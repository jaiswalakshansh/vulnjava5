package com.summersec.ssti.vuldemo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ClassName: sstidemo
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/8/24 10:47
 * @Version: v1.0.0
 * @Description:
 **/
@Controller
public class sstidemo {
    @RequestMapping(value = "/ssti")
    public String ssti(@RequestParam  String poc){
        return "" + poc;
    }


}
