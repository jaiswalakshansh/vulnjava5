package com.summersec.cb.LazyMap;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
/**
 * @ClassName: ExampleApp
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/5/25 16:43
 * @Version: v1.0.0
 * @Description: Java动态代理
 **/

public class ExampleApp {
    public static void main(String[] args) throws Exception {
        InvocationHandler handler = new ExampleInvocationHandler(new HashMap());
        Map proxyMap = (Map) Proxy.newProxyInstance(Map.class.getClassLoader(), new Class[] {Map.class}, handler);
        proxyMap.put("hello", "world");
        String result = (String) proxyMap.get("hello");
        System.out.println(result);
    }
}
