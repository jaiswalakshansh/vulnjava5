package com.summersec.cb.LazyMap;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @ClassName: ExampleInvocationHandler
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/5/25 16:39
 * @Version: v1.0.0
 * @Description: 动态代理修改值
 **/
public class ExampleInvocationHandler implements InvocationHandler {
    protected Map map;
    public ExampleInvocationHandler(Map map) {
        this.map = map;
    }
    // 修改hello对应的值
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().compareTo("get") == 0) {
            System.out.println("Hook method: " + method.getName());
            return "Hacked Object";
        }
        return method.invoke(this.map, args);
    }
}
