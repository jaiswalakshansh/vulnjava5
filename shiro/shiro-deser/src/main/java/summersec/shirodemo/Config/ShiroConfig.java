package summersec.shirodemo.Config;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import summersec.shirodemo.Realm.MyRealm;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @ClassName: ShiroConfig
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/1/8 15:28
 * @Version: v1.0.0
 * @Description: shiro配置类
 **/
@Configuration
public class ShiroConfig {
    @Bean
    MyRealm myRealm() {
        return new MyRealm();
    }

    @Bean
    DefaultWebSecurityManager securityManager() {
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        manager.setRememberMeManager(rememberMeManager());
        manager.setRealm(myRealm());
        return manager;
    }

    @Bean
    public RememberMeManager rememberMeManager(){
        CookieRememberMeManager cManager = new CookieRememberMeManager();
        cManager.setCipherKey(Base64.decode("4AvVhmFLUs0KTA3Kprsdag=="));
        //    用户信息保存在cookie中
        SimpleCookie cookie = new SimpleCookie("rememberMe");

        //    保存时间
        cookie.setMaxAge(7 * 24 * 60 * 60);
        cManager.setCookie(cookie);
        return cManager;
    }
    /**
     * anon：匿名用户可访问
     * authc：认证用户可访问
     * user：使用rememberMe可访问
     * perms：对应权限可访问
     * role：对应角色权限可访问
     */
    @Bean
    ShiroFilterFactoryBean shiroFilterFactoryBean() {
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        bean.setSecurityManager(this.securityManager());
//        bean.setLoginUrl("/login");
//        bean.setUnauthorizedUrl("/unauth");
//        bean.setSuccessUrl("/success");

        Map<String, String> map = new LinkedHashMap();
//        map.put("/doLogin", "anon");//* anon：匿名用户可访问
//        map.put("/login", "anon");//* anon：匿名用户可访问
//        map.put("/index", "anon");//* anon：匿名用户可访问
//        map.put("/static/**", "anon");//* anon：匿名用户可访问
//        map.put("/static/css/*", "anon");//* anon：匿名用户可访问
//        map.put("/static/js/*", "anon");//* anon：匿名用户可访问
//        map.put("/static/images/*", "anon");//* anon：匿名用户可访问
        map.put("/**", "anon");//* anon：匿名用户可访问
        map.put("/*", "anon");//* anon：匿名用户可访问

        map.put("/index/**", "anon"); //authc：认证用户可访问
        map.put("/index/*", "anon"); //authc：认证用户可访问
        bean.setFilterChainDefinitionMap(map);
        return bean;
    }
    /**
     * @Description: 修改rememberMe关键字
     * @param
     *
     * @return: simpleCookie
     */
//    @Bean
//    public SimpleCookie rememberMeCookie(){
//        SimpleCookie simpleCookie = new SimpleCookie("helloMe");
//        simpleCookie.setMaxAge(2592000);
//        return simpleCookie;
//    }


}
