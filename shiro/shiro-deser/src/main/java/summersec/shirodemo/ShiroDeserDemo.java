package summersec.shirodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: ShiroDeserDemo
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/1/8 15:29
 * @Version: v1.0.0
 * @Description: 启动程序
 **/
@SpringBootApplication
public class ShiroDeserDemo {
    public static void main(String[] args) {
        SpringApplication.run(ShiroDeserDemo.class,args);
    }
}
