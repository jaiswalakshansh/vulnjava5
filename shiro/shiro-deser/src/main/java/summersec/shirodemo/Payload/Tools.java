package summersec.shirodemo.Payload;

import java.io.*;
import java.nio.charset.Charset;

/**
 * @ClassName: Tools
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/1/18 13:37
 * @Version: v1.0.0
 * @Description: 命令执行类
 **/
public class Tools {

    public  String exeCmd(String commandStr) {
        BufferedReader br = null;
        String OS = System.getProperty("os.name").toLowerCase();


        try {
            InputStream stream;
            if (OS.startsWith("win")){
                stream = (new ProcessBuilder(new String[]{"cmd.exe", "/c", commandStr})).start().getInputStream();
            }else {
                stream = (new ProcessBuilder(new String[]{"/bin/bash", "-c", commandStr})).start().getInputStream();

            }

            InputStreamReader streamReader = new InputStreamReader(stream, Charset.forName("UTF-8"));
            BufferedReader bufferedReader = new BufferedReader(streamReader);
            StringBuffer buffer = new StringBuffer();
            String line = null;
            int flag =  0;

            while((line = bufferedReader.readLine()) != null) {
                if (flag == 0){
                    flag = 1;
                }else{
                    buffer.append(line);
                }

            }

            return buffer.toString();
        } catch (Exception var5) {
            var5.printStackTrace();
            return "error";
        }
    }
}
