package summersec.shirodemo.Payload;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @ClassName: DEMO
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/6/14 10:26
 * @Version: v1.0.0
 * @Description:
 **/
public class DEMO {
    public static void main(String[] args) throws CannotCompileException, IOException {
        ClassPool pool = ClassPool.getDefault();
//        Runtime.getRuntime().exec("calc.exe");
        CtClass ctClass = pool.makeClass("hello" + System.nanoTime());

        ctClass.addMethod(CtMethod.make("  public void hello(){ System.out.println(\"hello world!\"); Runtime.getRuntime().exec(\"calc.exe\");} \n",ctClass));
        try {
            Object ob = ctClass.toClass().newInstance();
            Method method = ob.getClass().getMethod("hello");
            method.invoke(ob);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


    }
}
