package summersec.shirodemo.Payload;

import java.io.*;
import java.util.Date;

/**
 * @ClassName: test
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/6/15 20:52
 * @Version: v1.0.0
 * @Description:
 **/
public class test {
    public static void main(String[] args) throws Exception {
        // Serialize today's date to a file.
        FileOutputStream f = new FileOutputStream("tmp");
        ObjectOutput s = new ObjectOutputStream(f);
        s.writeObject("Today");
        s.writeObject(new Date());
        s.flush();
        // Deserialize a string and date from a file.
        FileInputStream in = new FileInputStream("tmp");
        //反序列化时使用上面的CompatibleInputStream即可
        ObjectInputStream ois = new CompatibleInputStream(in);
        String today = (String)ois.readObject();
        Date date = (Date)ois.readObject();
    }
}
