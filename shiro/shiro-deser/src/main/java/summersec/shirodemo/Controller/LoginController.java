package summersec.shirodemo.Controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * @ClassName: UserController
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/1/8 15:28
 * @Version: v1.0.0
 * @Description:
 **/
@Controller
public class LoginController implements HandlerInterceptor {

    @RequestMapping({"/login"})
    public String Login(){
        return "Login";
    }

    @PostMapping({"/doLogin"})
    public String doLoginPage(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam(name = "rememberme",defaultValue = "") String rememberMe) {
        Subject subject = SecurityUtils.getSubject();

        try {
            subject.login(new UsernamePasswordToken(username, password, rememberMe.equals("rememberMe")));
            System.out.println("登录成功！");
            return "FORWARD:/success";
        } catch (AuthenticationException var6) {
            return "forward:/login";
        }
    }

    @RequestMapping({"/"})
    public String helloPage() {
        return "hello";
    }
//
    @RequestMapping({"/unauth"})
    public String errorPage() {
        return "error! unauth ...";
    }

    @RequestMapping("/success")
    public String success(){
        return "success auth";
    }




}
