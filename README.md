[![Github build](https://img.shields.io/badge/build-success-brightgreen)](https://img.shields.io/badge/build-success-brightgreen)[![GitHub issues](https://img.shields.io/github/issues/SummerSec/JavaLearnVulnerability)](https://github.com/SummerSec/JavaLearnVulnerability/issues)[![GitHub release](https://img.shields.io/github/release/SummerSec/JavaLearnVulnerability.svg)](https://github.com/SummerSec/JavaLearnVulnerability/releases)[![GitHub forks](https://img.shields.io/github/forks/SummerSec/JavaLearnVulnerability)](https://github.com/SummerSec/JavaLearnVulnerability)[![GitHub Followers](https://img.shields.io/github/followers/SummerSec.svg?style=social&label=Follow)](https://github.com/SummerSec/JavaLearnVulnerability/) [![Github Stars](https://img.shields.io/github/stars/SummerSec/JavaLearnVulnerability.svg?style=social&label=Stars)](https://github.com/SummerSec/JavaLearnVulnerability/)[![Follow on Twitter](https://img.shields.io/twitter/follow/SecSummers.svg)](https://twitter.com/intent/follow?screen_name=SecSummers)
![](https://visitor-badge.laobi.icu/badge?page_id=SummerSec.JavaLearnVulnerability)




# JavaLearnVulnerability

Java漏洞学习代码及笔记

# 项目TODO

- [x] 漏洞代码完善中
- [x] 漏洞使用和分析笔记准备中
    - [x] 目前文章分析地址在每一个包下package-info.java
- [x] Java反序列化
- [x] Java反射
- [x] Java类加载
- [x] shiro漏洞分析
- [x] weblogic漏洞分析
- [x] fastjson漏洞分析
- [ ] jackson漏洞分析
- [ ] rmi、ldap
- [ ] spring漏洞分析

目前所有文章分析都在博客 http://summersec.github.io 后期会转到项目下，所有的分析文章写完之后统一转入。

项目优势：

    1. 每一个漏洞环境都支持单独运行，方便分析
    2. 漏洞都有分析文章，完全可以参考文章分析进行
    3. 即使小白也能看懂
    4. 每一个部分从基础的知识学习，再到漏洞实战分析，漏洞实战分析主要以最近几年的历史漏洞。
    5. 由浅入深，由易到难。 



---
# 项目说明

| 项目     |           | 说明                                          |
| -------- | --------- | --------------------------------------------- |
| javatest |           | 依赖文件都在lib目录下，idea开箱即用           |
| vuldemo  |           | maven项目依赖需要下载，idea下载编译即可       |
| weblogic |           | maven项目，部分依赖需要下载，idea下载编译即可 |
| shiro    | shiro漏洞 | maven项目，idea下载编译即可                   |
| Rce_Echo | Java 回显 | maven项目，idea下载编译即可                   |

#  

----

 [![Stargazers over time](https://starchart.cc/SummerSec/JavaLearnVulnerability.svg)](https://starchart.cc/SummerSec/JavaLearnVulnerability) 







<img align='right' src="https://profile-counter.glitch.me/summersec/count.svg" width="200">

