<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FastJson漏洞演示程序</title>
    <style type="text/css">
        #textJson{
            width: 800px;
            margin-right: 5px;
            margin-top: 30px;
        }
        #logo{
            margin-top: 100px;
            text-align: center;
        }
        #img{
            width: 400px;
            height: 360px;
        }
    </style>
</head>
<body>
<div id="logo">
    <img src="/logo.jpg" id="img"/>
    <form action="parse" method="post">
        <input type="text" name="textJson" id="textJson" value="{'age':44,'name':'Bearcat','sex':'male'}">
        <input type="submit" value="提交">
    </form>
</div>
</body>
</html>
