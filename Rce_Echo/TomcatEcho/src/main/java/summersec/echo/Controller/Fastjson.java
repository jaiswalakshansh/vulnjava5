package summersec.echo.Controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * @ClassName: Fastjson
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/6/22 16:24
 * @Version: v1.0.0
 * @Description:
 **/
@Controller
public class Fastjson {
    @RequestMapping(value = "fastjson")
    public String fastjsonvul(){
        return "fastjson";
    }

    @RequestMapping(value = "parse")
    public void vuldemo1248(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String result = request.getParameter("textJson");
        System.out.println(result);
        try {
            JSON.parse(result);
        }catch (Exception e) {
            response.getWriter().println(e);
        }
        response.getWriter().println("parse is ok");
        response.getWriter().flush();
        response.getWriter().close();
    }





}
