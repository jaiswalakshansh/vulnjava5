package summersec.echo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName: TestECHO
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/12/10 15:13
 * @Version: v1.0.0
 * @Description:
 **/
@Controller
public class TestECHO {
    @RequestMapping("/allecho")
    public void echo(HttpServletRequest request, HttpServletResponse response) {
         new AllEcho();
    }

}
