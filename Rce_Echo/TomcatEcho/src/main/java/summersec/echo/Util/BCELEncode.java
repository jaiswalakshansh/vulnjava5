package summersec.echo.Util;

import com.sun.org.apache.bcel.internal.classfile.Utility;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @ClassName: BCELEncode
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/6/26 16:00
 * @Version: v1.0.0
 * @Description:
 **/
public class BCELEncode {
    public static String class2BCEL(String classFile) throws Exception{
        Path path = Paths.get(classFile);
        byte[] bytes = Files.readAllBytes(path);
        String result = Utility.encode(bytes,true);
        return result;
    }
}
