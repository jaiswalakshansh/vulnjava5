package summersec.echo.Util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @ClassName: CMDStr
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/4/28 17:29
 * @Version: v1.0.0
 * @Description:
 **/
public class CMDStr {

    public static String exeCmd(String commandStr) {
        BufferedReader br = null;
        String OS = System.getProperty("os.name").toLowerCase();


        try {
            Process p = null;
            if (OS.startsWith("win")){
                p = Runtime.getRuntime().exec(new String[]{"cmd", "/c", commandStr});
            }else {
                p = Runtime.getRuntime().exec(new String[]{"/bin/bash", "-c", commandStr});
            }

            br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            StringBuilder sb = new StringBuilder();

            while((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }

            return sb.toString();
        } catch (Exception var5) {
            var5.printStackTrace();
            return "error";
        }
    }
}
