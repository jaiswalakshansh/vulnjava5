package summersec.echo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @ClassName: ShiroDemoApplication
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/4/28 16:24
 * @Version: v1.0.0
 * @Description:
 **/
@SpringBootApplication
public class TomcatEchoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TomcatEchoApplication.class, args);
    }

}
