package summersec.echo.Demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import summersec.echo.Controller.TomcatEcho;

import java.lang.reflect.Field;

/**
 * @ClassName: DEMo
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/6/16 17:56
 * @Version: v1.0.0
 * @Description:
 **/
//@Controller
public class DEMO {
    static class User {
        public String name = " asd";
        public TomcatEcho tomcatEcho = null;

        public String getName() {
            return name;
        }

        public TomcatEcho getTomcatEcho() {
            return tomcatEcho;
        }

        public void setTomcatEcho(TomcatEcho tomcatEcho) {
            this.tomcatEcho = tomcatEcho;
        }

        public void setName(String name) {
            this.name = name;
        }

        public User(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
    public static void main(String[] args) {
        User user = new User("sadqwe");
        try {
            Field name = user.getClass().getDeclaredField("tomcatEcho");
            Object ob =  name.get(user.getClass());
            String s = ob.getClass().getName();
            System.out.println(s);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }


    }

}
