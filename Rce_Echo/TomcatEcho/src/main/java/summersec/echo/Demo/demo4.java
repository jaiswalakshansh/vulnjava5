package summersec.echo.Demo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;

public class demo4 extends ClassLoader{

    public Class g(byte[] b){
        return super.defineClass(b,0,b.length);
    }

    public static void main(String[] args) {
        File file = new File("src/main/java/java/lang/demo3.class");
        try {
            byte[] bytes = getClassBytes(file);
            demo4 demo4 =new demo4();
            Class c = demo4.g(bytes);
            c.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private static byte[] getClassBytes(File file) throws Exception
    {
        // 这里要读入.class的字节，因此要使用字节流
        FileInputStream fis = new FileInputStream(file);
        FileChannel fc = fis.getChannel();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        WritableByteChannel wbc = Channels.newChannel(baos);
        ByteBuffer by = ByteBuffer.allocate(1024);

        while (true){
            int i = fc.read(by);
            if (i == 0 || i == -1)
                break;
            by.flip();
            wbc.write(by);
            by.clear();
        }
        fis.close();
        return baos.toByteArray();
    }

}
