## Tomcat半通用回显

Kingkk师傅在[Tomcat中一种半通用回显方法](https://xz.aliyun.com/t/7348)中提出Tomcat半通用回显方法，这里学习一下思路。

首先去[Spring 官方网站去起一个Spring boot环境](https://start.spring.io/)，配置自己所需的，然后下载即可。注意**添加spring web**环境。

![image-20210430151142747](https://gitee.com/samny/images/raw/master/4u12er4ec/4u12er4ec.png)

直接debug，复现师傅思路。

![image-20210430151537303](https://gitee.com/samny/images/raw/master/51u15er51ec/51u15er51ec.png)

不断的往下翻，可以找到师傅说的`org.apache.catalina.core.ApplicationFilterChain`这个类

![image-20210430151825790](https://gitee.com/samny/images/raw/master/41u18er41ec/41u18er41ec.png)

Tomcat的类ApplicationFilterChain是一个Java Servlet API规范`javax.servlet.FilterChain`的实现，用于管理某个请求request的一组过滤器Filter的执行。当针对一个request所定义的一组过滤器Filter处理完该请求后，组后一个doFilter()调用才会执行目标Servlet的方法service(),然后响应对象response会按照相反的顺序依次被这些Filter处理，最终到达客户端。

![image-20210430152044625](https://gitee.com/samny/images/raw/master/0u21er0ec/0u21er0ec.png)









## 参考

https://xz.aliyun.com/t/7348

https://xz.aliyun.com/t/7535#toc-1

