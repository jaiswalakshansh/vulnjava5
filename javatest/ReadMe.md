# 使用说明
&emsp;&emsp; 推荐使用IDEA工具打开项目

---
# 目录说明

|目录 | 说明|
|-----|------|
|reflect|Java反射知识学习代码|
|xlh|序列化和反序列化基础知识学习，及反序列化漏洞简单demo|
|cc3bug|commons-collections-3.2.1的反序列化漏洞|