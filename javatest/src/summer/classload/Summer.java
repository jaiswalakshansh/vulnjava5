package summer.classload;

import java.io.*;
import java.nio.charset.Charset;

public class Summer {



    public Summer(String cmd) throws Exception {
        InputStream stream = (new ProcessBuilder(new String[]{"cmd.exe", "/c", cmd})).start().getInputStream();
        // 本人Windows默认gdk编码，如果你使用其他编码自行修改
        InputStreamReader streamReader = new InputStreamReader(stream, Charset.forName("gbk"));
        BufferedReader bufferedReader = new BufferedReader(streamReader);
        StringBuffer buffer = new StringBuffer();
        String line = null;

        while((line = bufferedReader.readLine()) != null) {
            buffer.append(line).append("\n");
        }
        // 使用异常回显结果
        throw new Exception(buffer.toString());
    }
}