package summer.classload;


import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @ClassName: demo
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/5/26 15:29
 * @Version: v1.0.0
 * @Description: 利用反射方法在线demo类加载的
 **/
public class demo {
    public static void main(String[] args) throws Exception {

        URL url = new URL("http://127.0.0.1:8090/summer.jar");
//        URL url = new URL("file:e:/summer.jar");

        URLClassLoader ucl = new URLClassLoader(new URL[]{url});
        Class cls = ucl.loadClass("Summer");
        Method m = cls.getMethod("Exec",String.class);
        m.invoke(cls.newInstance(),"whoami");


    }
}
