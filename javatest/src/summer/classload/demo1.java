package summer.classload;

import summer.util.FiletoBytes;

import java.io.File;

/**
 * @ClassName: demo1
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/5/26 15:49
 * @Version: v1.0.0
 * @Description:
 **/
public class demo1 {
    // Summer.class的绝对路径
    // E:\Soures\JavaLearnVulnerability\Summer.class自行修改
    static final String file_name = "E:\\Soures\\JavaLearnVulnerability\\javatest\\out\\production\\javabug\\summer\\classload\\Summer.class";
    static final byte[] bytes = new byte[1502];

    public static void main(String[] args) {
        FiletoBytes filetoBytes = new FiletoBytes(file_name);
//        System.out.println("直接传入文件，默认bytes大小默认为4kb");
//        System.out.println(filetoBytes.FiletoBytes(file_name));
        System.out.println("传入文件和bytes");
        System.out.println(filetoBytes.FiletoBytes(file_name,bytes));

    }

}
