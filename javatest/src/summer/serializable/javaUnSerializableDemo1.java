package summer.serializable;



import java.io.*;
import java.util.Date;

/**
 * @Author: summer
 * @Date: 2020/4/22 14:15
 * @Version: 1.0.0
 * @Description:
 *
 */

public class javaUnSerializableDemo1 {
    public static void main(String[] args) {
        javaSerializableDemo1 demo = new javaSerializableDemo1("Summer","6666888",18,666666,new Date());

        System.out.println("serializable: " + demo);

        // 将对象写入文件中
        ObjectOutputStream oos = null;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("tempFile.bin");
            oos = new ObjectOutputStream(fileOutputStream);

            // 序列化
            oos.writeObject(demo);

            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 读文件
        File file = new File("tempFile.bin");
        ObjectInputStream ois = null;
        try {

            FileInputStream fileInputStream = new FileInputStream(file);
            ois = new ObjectInputStream(fileInputStream);

            // 反序列化
            javaSerializableDemo1 newdemo = (javaSerializableDemo1) ois.readObject();
            System.out.println("unserializable: " + newdemo);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
