package summer.serializable;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: Samny
 * @Date: 2020/4/22 15:23
 * @Version: 1.0.0
 * @Description: 创建一个实例类
 */

public class javaSerializableDemo2 implements Serializable {

    private static final long serialVersionUID = 5873658113503875074L;
    // 序列化版本ID

    private Integer age;
    private String username;
    private String password;
    private Date date;
    private String id;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }



    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "javaSerializableDemo2{" +
                "age=" + age +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", date=" + date +
                ", id='" + id + '\'' +
                '}';
    }
}
