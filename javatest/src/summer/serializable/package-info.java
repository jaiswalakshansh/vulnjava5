/**
 * @Author: Samny
 * @Date: 2020/4/22 16:10
 * @Version: 1.0.0
 * @Description: 基础序列化和反序列化知识示例
 *
 */
//https://summersec.github.io/2020/05/13/%E4%BB%8E%E5%AE%89%E5%85%A8%E8%A7%92%E5%BA%A6%E8%B0%88Java%E5%8F%8D%E5%B0%84%E6%9C%BA%E5%88%B6--%E7%BB%88%E7%AB%A0/
//https://summersec.github.io/2020/05/12/%E4%BB%8E%E5%AE%89%E5%85%A8%E8%A7%92%E5%BA%A6%E8%B0%88Java%E5%8F%8D%E5%B0%84%E6%9C%BA%E5%88%B6--%E5%BA%8F%E7%AB%A0/
//https://summersec.github.io/2020/05/12/%E4%BB%8E%E5%AE%89%E5%85%A8%E8%A7%92%E5%BA%A6%E8%B0%88Java%E5%8F%8D%E5%B0%84%E6%9C%BA%E5%88%B6--%E5%89%8D%E7%AB%A0/
package summer.serializable;