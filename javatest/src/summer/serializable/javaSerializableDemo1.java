package summer.serializable;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: Summer
 * @Date: 2020/4/22 14:10
 * @Version: 1.0.0
 * @Description  不常使用的方法
 */
public class javaSerializableDemo1 implements Serializable {

    // 序列版本ID
    private static final long serialVersionUID = -1877568378649280904L;
    private String username;
    private String password;
    private Integer age;
    private Integer IdCard;
    private Date time;




    public javaSerializableDemo1(String username, String password, Integer age, Integer idCard, Date time) {
        this.username = username;
        this.password = password;
        this.age = age;
        IdCard = idCard;
        this.time = time;
    }

    public javaSerializableDemo1() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static long getUId() {
        return serialVersionUID;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getIdCard() {
        return IdCard;
    }

    public void setIdCard(Integer idCard) {
        IdCard = idCard;
    }

    @Override
    public String toString() {
        return "javaSerializableDemo{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                ", IdCard=" + IdCard +
                ", time=" + time +
                '}';
    }
}
