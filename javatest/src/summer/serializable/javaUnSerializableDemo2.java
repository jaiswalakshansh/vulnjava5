package summer.serializable;

import java.io.*;
import java.text.MessageFormat;
import java.util.Date;

/**
 * @Author: Samny
 * @Date: 2020/4/22 15:31
 * @Version: 1.0.0
 * @Description: 序列化和反序列化对象
 */

public class javaUnSerializableDemo2 {
    private static Object javaSerializableDemo2;

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        // 序列化对象
        Serializable();
        // 反序列化对象
        javaSerializableDemo2 demo2 = UnSerializable();

        System.out.println(MessageFormat.format("age={0},username={1}, password={2}, date={3}, " +
                "id={4}",demo2.getAge(),demo2.getUsername(),demo2.getPassword(),
                demo2.getDate(),demo2.getId()));
    }

    public static void Serializable() {
        javaSerializableDemo2 demo2 = new javaSerializableDemo2();
        demo2.setAge(18);
        demo2.setId("2121");
        demo2.setUsername("summer");
        demo2.setPassword("1234555");
        demo2.setDate(new Date());

        // 将对象写入文件中
        File file = new File("tempFile2");
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            // 序列化
            oos.writeObject(demo2);
            System.out.println("serializable: " + demo2);
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static javaSerializableDemo2 UnSerializable(){

        File file = new File("tempFile2");
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            // 反序列化
            javaSerializableDemo2 demo2 = (javaSerializableDemo2) ois.readObject();
            System.out.println("unserializable: " + demo2 );

            return demo2;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return (summer.serializable.javaSerializableDemo2) javaSerializableDemo2;
    }


}
