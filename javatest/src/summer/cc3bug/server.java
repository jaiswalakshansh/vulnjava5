package summer.cc3bug;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName: server
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/23 16:18
 * @Version: v1.0.0
 * @Description: 服务器端
 * https://samny.gitee.io/2020/05/26/%E6%BC%AB%E8%B0%88Commons-Collections%E5%8F%8D%E5%BA%8F%E5%88%97%E5%8C%96/
 **/
public class server {
    public static void main(String[] args) {
        // 模拟服务器端，接受反序列化数据
        try {
            ServerSocket serverSocket = new ServerSocket(6666);
            System.out.println("服务器监听地址： " + serverSocket.getLocalSocketAddress());
            while (true){
                // 接受反序列化数据

                Socket socket = serverSocket.accept();
                System.out.println("与地址： " + socket.getInetAddress() + "连接！" );
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                try {
                    // 读取数据
                    Object ob = ois.readObject();
                    System.out.println("读取数据完成！");
                    System.out.println(ob);

                } catch (ClassNotFoundException e) {
                    System.out.println("读取数据失败！");
                    e.printStackTrace();

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
