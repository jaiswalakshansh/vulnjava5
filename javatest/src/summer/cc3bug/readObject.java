package summer.cc3bug;

import java.io.*;

/**
 * @ClassName: readObject
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/5/18 10:07
 * @Version: v1.0.0
 * @Description: 读取反序列化文件
 **/
public class readObject {
    public static void main(String[] args) {
        File file = new File("tempFile2");
        FileInputStream fis = null;

        try {
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ois.readObject();
            ois.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
