package summer.reflection;

/**
 * @ClassName: Students
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/22 19:04
 * @Version: v1.0.0
 * @Description: 学生个人类
 **/
public class Students {
    private Integer age = 18;
    private String name = "summer";
    private Integer CardId = 332323223;
    private Integer id = 10012;
    private String hello = "hello world";

//    public static void main(String[] args) {
//        Students students = new Students();
//        String s = students.toString();
//        System.out.println(s);
//    }
    public Students(Integer age, String name, Integer cardId, Integer id, String hello) {
        this.age = age;
        this.name = name;
        this.CardId = cardId;
        this.id = id;
        this.hello = hello;

    }


    @Override
    public String toString() {
        return "Students{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", CardId=" + CardId +
                ", id=" + id +
                ", ags='" + hello + '\'' +
                '}';
    }

    public Students() {
//        this.age = 12;
//        this.name = "name";
//        this.CardId = 12112;
//        this.id = 101;
//        this.hello = "hello";
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCardId() {
        return CardId;
    }

    public void setCardId(Integer cardId) {
        CardId = cardId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAgs() {
        return hello;
    }

    public void setAgs(String hello) {
        this.hello = hello;
    }
}
