package summer.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @ClassName: calc
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/22 19:23
 * @Version: v1.0.0
 * @Description: 继承Students类，使用反射的方法弹出计算器
 **/
public class reflectdemo1 {

    public static void main(String[] args) {
//        System.out.print("reflectmethod1：");
//        reflectmethod1();
        System.out.println("reflectmethod2: ");
        reflectmethod2();
//        System.out.print("common method：");
//        Sts();

    }
    /**
     * @Author:         samny
     * @CreateDate:     2020/4/22 19:37
     * @UpdateUser:     samny
     * @UpdateDate:     2020/4/22 19:37
     * @UpdateRemark:   修改内容
     * @Version:        v1.0.0
     * @Description:    反射方法获取Students类
     */
    public static void reflectmethod1(){
        try {

            // 获取对象

            Class cls = Class.forName("summer.reflection.Students");

//            // 获取构造器方法
            Class[] classes = new Class[]{Integer.class,String.class,
                    Integer.class,Integer.class,String.class};
            Constructor constructor = cls.getDeclaredConstructor(classes);

            Object str = constructor.newInstance(18, "summer", 32326663, 10021, "hello!!");
            System.out.println(str);
            // 或者下面这样子输出
//            System.out.println(constructor.newInstance(18, "samny", 32326663, 10021, "hello!!"));

        } catch (ClassNotFoundException | NoSuchMethodException |
                IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    /**
     * @Author:         samny
     * @CreateDate:     2020/4/23 13:27
     * @UpdateUser:     samny
     * @UpdateDate:     2020/4/23 13:27
     * @UpdateRemark:   修改内容
     * @Version:        v1.0.0
     * @Description:    有参和无参方法的反射调用
     */
    public static void reflectmethod2(){
        try {
            Class  cls = Class.forName("summer.reflection.classdemo1");
            // 无参方法调用
            Object ob = cls.newInstance();
//            Method mt = cls.getMethod("print"); 有没有null均可
            Method mt = cls.getMethod("print",null);
            mt.invoke(ob,null);
            // 有参方法调用
            Method mt2 = cls.getMethod("print2", String.class);
            mt2.invoke(ob,"world");


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Author:         samny
     * @CreateDate:     2020/4/22 22:06
     * @UpdateUser:     samny
     * @UpdateDate:     2020/4/22 22:06
     * @UpdateRemark:   修改内容
     * @Version:        v1.0.0
     * @Description:    常规方法实例化对象输出对象
     */
    public static void Sts(){
        Students students = new Students();
        students.setAge(18);
        students.setName("summer");
        students.setCardId(32336666);
        students.setId(1001);
        students.setAgs("helloooo!!");
        System.out.println(students.toString());



    }
}
