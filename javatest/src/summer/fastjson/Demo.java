package summer.fastjson;

import com.alibaba.fastjson.JSON;
import summer.reflection.Students;

/**
 * @ClassName: Demo
 * @Description: TODO
 * @Author: Summer
 * @Date: 2020/6/20 17:00
 * @Version: v1.0.0
 * @Description:
 **/
public class Demo {
    public static void main(String[] args) {
        // 示例类
        Students students = new Students();

        // 序列化
        String serstr = JSON.toJSONString(students);
        System.out.println("序列化；  " + serstr);

        // 使用parse方法反序列化
        Object ob = JSON.parse(serstr);
        System.out.println("parse反序列化对象：" + ob);
        System.out.println("parse反序列化对象名称：" + ob.getClass().getName());

        // 使用parseObject方法反序列化
        Object ob1 = JSON.parseObject(serstr);
        System.out.println("parseObject反序列化对象：" + ob1);
        System.out.println("parseObject反序列化对象名称：" + ob1.getClass().getName());

        //使用parseObject反序列化 返回一个对应的类对象
        Object ob2 = JSON.parseObject(serstr,Students.class);
        System.out.println("parseObject反序列化对象： " + ob2);
        System.out.println("parseObject反序列化对象名称： " + ob2.getClass().getName());


    }
}
