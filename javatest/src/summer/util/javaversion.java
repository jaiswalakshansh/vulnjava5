package summer.util;

/**
 * @ClassName: javaversion
 * @Description: TODO
 * @Author: Samny
 * @Date: 2020/5/6 19:33
 * @Version: v1.0.0
 * @Description:
 **/
public class javaversion {

    public int major;
    public int minor;
    public int update;


    public static javaversion getLocalVersion () {
        String property = System.getProperties().getProperty("java.version");
        if (property == null) {
            return null;
        }
        javaversion v = new javaversion();
        String parts[] = property.split("\\.|_|-");
        int start = "1".equals(parts[0]) ? 1 : 0; // skip "1." prefix
        v.major = Integer.parseInt(parts[start + 0]);
        v.minor = Integer.parseInt(parts[start + 1]);
        v.update = Integer.parseInt(parts[start + 2]);
        return v;
    }

    public static boolean isAnnInvHUniversalMethodImpl () {
        javaversion v = javaversion.getLocalVersion();
        return v != null && (v.major < 8 || (v.major == 8 && v.update <= 71));
    }

    public static boolean isBadAttrValExcReadObj () {
        javaversion v = javaversion.getLocalVersion();
        return v != null && (v.major > 8 && v.update >= 76);
    }

    public static boolean isAtLeast ( int major){
        javaversion v = javaversion.getLocalVersion();
        return v != null && v.major >= major;
    }


}
